//Callback merupakan parameter yang berupa medtod/fungsi yang dapat digunakan untuk membuat asynchronous dengan callback karena callback itu bekerja secara asynchronous ,yang dapat mempercepat performace. Callback juga di sebut sebagai Higer Order Function(contoh method yang ada di javasccript :terdapat map, reduce, filter, dll) atau Callback sebauh fungsi yang di jadikan sebagai parameter

//Peroedaan Callback dengan Fungsi pd umunya, jika fungsi pda umunya di eksekusi secara langsung sedangkan callback di eksekusi dalam function lain sebagai parameter karena yang mengekseusi function adalah function yang di panggil bukan kita sendiri yang memanggil fungsinya

//Dibutuhkan ketika menggunakan Event Listener, Menangani proses asynchronous, ketika akan melakukan injeksi atau modifikasi hasil eksekusi dri sebuah function

function main (param1, param2, cb){
    console.log(param1, param2);
    cb();
}
function myCallback(){
    console.log('hello callback');
}
main(1,2, myCallback)

//function injeksi, kita bisa manipulasi fungsi kita sendiri,
//Alasan knp fungsi bisa dijadikan paraameter karena itu yang di sebut first-class-object (function di javascript adalah object)

//contoh injeksi atau modifikasi hasil eksekusi sebuah function
//function injeksi adalah kita dapat manipulasi fungsi kita sendiri
//callback salah satu fungsi sebagai injeksi, bisa melakukan injeksi sebuah poses di dalam proses 
//test
function calculate(param1, param2, callback){
    let result = param1 + param2;
    if(typeof callback === "function"){
        result = callback(param1,param2);
    }
    return result;
}
//test
let a = calculate(7000, 2000, function(x,y){
    return x * y;
})
let b = calculate(3000,2000);
console.log(a)
console.log(b)



//memanfatkan callback pada asynchronous untuk ajax ative hit api
//fungsi fetch API (native js) mengatikan ajax native
// function getData(url, callback){
//     let xhr = new XMLHttpRequest();
//     xhr.onload = function(){
//         if(xhr.status == 200){
//             return callback(JSON.parse(xhr.responseText))
//         }
//     };
//     xhr.open("GET", url);
//     xhr.send();
// }
// //mengunakan param cb untuk meganti problem
// const data = getData('https://jsonplaceholder.typicode.com/users/1', function(data){
//     console.log(data)
// });
// console.log(data)

class Table {
    constructor(init){
        this.init = init;
    }
    createHeader(data){
        let open = "<thead><tr>";
        let close = "</tr></thead>";
        data.map((d, index)=>{
            open += `<th key=${index}>${d}</th>`
        })
        return open + close;
    }
    createBody(data){
        let open = "<tbody id='table-body'>";
        let close = "</body>";
        data.map((d, index)=>{
            open += `
            <tr key=${index}>
                <td>${d.id}</td>
                <td>${d.name}</td>
                <td>${d.username}</td>
                <td>${d.email}</td>
                <td>${d.address.street}, ${d.address.suite}, ${d.address.city}</td>
                <td>${d.company.name}</td>
            </tr>`;
        })
        // data.map((d, index)=>{
        // })
        console.log(data)   
        return open + close;
    }
    render(element){        
        let tabel = `
        <table id="data-table" class="table table-success table-striped">
        </table>`
        element.innerHTML = tabel;
    }
    // search(input){
    //     const filtered = this.init.data.filter(d => d.name.toLowerCase().includes(input.toLowerCase()));
    //     const table = document.getElementById('data-table');
    //     console.log('table el', table);
    //     const tbody = document.getElementById('table-body');
    //     console.log('table body', tbody);
    //     table?.removeChild(tbody);
    //     if(filtered.length > 0){
    //         const newBody = document.createElement('tbody');
    //         newBody.setAttribute('id', 'table-body');
    //         const rows = `
    //             ${filtered.map((d, index)=>{
    //                 `<tr key=${index}>
    //                     <td>${d.id}</td>
    //                     <td>${d.name}</td>
    //                     <td>${d.username}</td>
    //                     <td>${d.email}</td>
    //                     <td>${d.address.street}, ${d.address.suite}, ${d.address.city}</td>
    //                     <td>${d.company.name}</td>
    //                 </tr>`;
    //             })}
    //         `
    //         newBody.innerHTML = rows.replace(",", "");//digunakan untuk menganti tanda , menjadi string kosong.
    //         if(table) table.appendChild(newBody);
    //     }else{
    //         const newBody = document.createElement('tbody');
    //         newBody.setAttribute('id', 'table-body');
    //         const rows = `
    //             ${this.init.data.map((d, index)=>{
    //                 `<tr key=${index}>
    //                     <td>${d.id}</td>
    //                     <td>${d.name}</td>
    //                     <td>${d.username}</td>
    //                     <td>${d.email}</td>
    //                     <td>${d.address.street}, ${d.address.suite}, ${d.address.city}</td>
    //                     <td>${d.company.name}</td>
    //                 </tr>`
    //             })}
    //         `
    //         newBody.innerHTML = rows.replace(',', "");
    //         if(table) table.appendChild(newBody);
    //     }
    // }
}
// let load = false;

// console.log(loading);
function getData(url, callback){
    let tabel = document.getElementById('data-table');
    let loading = document.getElementById('load');
    //callback sebagai event listener
    // document.getElementById('btn').innerHTML = "Loading ....";
    // document.getElementById("btn").setAttribute('disabled', 'true');
    let xhr = new XMLHttpRequest();
    xhr.onload = function(){
        // load = true;
        tabel.style.display= "none";
        loading.style.display="block";
        // let Loading = addClass('load');
        if(xhr.status == 200){
            tabel.style.display= "block"
            loading.style.display="none"
            const cb = callback(JSON.parse(xhr.responseText))
            return cb;
            // Loading.addClass('load-none');
            // tabel = document.getElementById('data-table');
        }
    };
    xhr.open('GET', url);
    xhr.send()
}
const newTable = new Table({
    columns : ['No', 'Nama', 'Username', 'Email', 'Alamat Lengkap', 'Perusahaan'],
    data : [],
});
const App = document.getElementById('App');
newTable.render(App)

const dataAPI = getData('https://jsonplaceholder.typicode.com/users', function(dataAPI){
    // console.log(dataAPI)
    const tabel = new Table({
        columns : ['No', 'Nama', 'Username', 'Email', 'Alamat Lengkap', 'Perusahaan'],
        data : dataAPI
    });
    // var newData = tabel.init.data;
    // console.log(tabel.init.data)
    const App = document.getElementById('App');
    // const input = document.getElementById('search-bar');
    // console.log(App)
    tabel.render(App)
    // input.addEventListener('change', (event)=>{
    //     console.log('search value', event.target.value);
        // tabel.search(event.target.value)
    // })
})