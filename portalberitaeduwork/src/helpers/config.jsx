/** header untuk public api */
export const publicHeader = () => {
  return {
    // "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  };
};

export const tokenHeader = () => {
  return {
    // "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
    Authorization: `e3ab8519e993408195acf9895d507d1e`,
  };
};

export const url = {
  /** dengan localhost dan port jika sedang menjalankan di local */
  // app: process.env.REACT_APP_APP_URL,
  app: process.env.REACT_APP_APP_URL,
  api: process.env.REACT_APP_API_URL,
};
