import { Routes, Route } from "react-router-dom";
import "./App.css";
import Users from "./customHook/Users";
import Karyawan from "./customHook/Users";
import Home from "./view/Home";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/users" element={<Users />} />
      </Routes>
    </>
  );
}

export default App;
