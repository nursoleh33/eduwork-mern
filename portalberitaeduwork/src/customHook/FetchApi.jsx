import React, { useEffect, useState } from "react";
import axios from "axios";
import { tokenHeader, url, publicHeader } from "../helpers/config.jsx";

const useFetchApi = () => {
  const hdr = tokenHeader();

  // let [query, setQuery] = useState(0);
  let [data, setData] = useState([]);
  let [loading, setLoading] = useState(false);
  let [error, setError] = useState(null);
  const getData = async (url) => {
    setLoading(true);
    await axios({
      method: "GET",
      url: url,
      headers: hdr,
      data: {},
    })
      .then((res) => {
        console.log(res);
        if (res.data == 200) {
          let data = res.data;
          setData(data);
          setLoading(false);
        }
      })
      .catch((err) => {
        setError(err);
        setLoading(false);
      });
  };
  useEffect(() => {
    getData();
  }, []);

  return [data, loading, error];
};

export default useFetchApi;
