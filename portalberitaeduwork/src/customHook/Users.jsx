import React, { useEffect } from "react";
import useAxios from "./useAxios";
import { publicHeader } from "../helpers/config.jsx";

const Users = () => {
  const hdr = publicHeader();
  let [loading, data, error, karyawan] = useAxios({
    method: "GET",
    url: "https://jsonplaceholder.typicode.com/users",
    headers: hdr,
    body: {},
  });
  //untuk mengatasi ketika state belum selesai simpan data
  useEffect(() => {
    console.log("karyawan", karyawan);
  }, [karyawan]);
  return (
    <>
      <h1>
        {karyawan.map((value) => {
          return (
            <>
              <ul key={value.id}>
                <li>
                  {value.name} - {value.email}
                </li>
              </ul>
            </>
          );
        })}
      </h1>
    </>
  );
};

export default Users;
