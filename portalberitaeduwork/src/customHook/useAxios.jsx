import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";

const useAxios = ({ url, config, type }) => {
  let [loading, setLoading] = useState(false);
  // cukup buat 1 state untuk menampg hasil response API walupun base url beda
  let [data, setData] = useState(null);
  let [karyawan, setKaryawan] = useState([]);
  let [error, setError] = useState("");
  const sendRequest = useCallback(() => {
    setLoading(true);
    axios({
      url,
      ...config,
    })
      .then((response) => {
        setKaryawan(response.data);
        setData(response.data.articles);
        setLoading(false);
        // Swal("Success", "Data Berhasil Di tampilkan", "success");
      })
      .catch((error) => {
        setError(error.messege);
        setLoading(false);
        // Swal("Sorry", "Data Gagal Di tampilkan", "error");
      });
  }, [config, url]);
  useEffect(() => {
    sendRequest();
  }, [url]);
  // useEffect(() => {
  //   console.log(data);
  // }, [data]);
  // useEffect(() => {
  //   console.log(karyawan);
  // }, [karyawan]);
  return { loading, data, error };
};

export default useAxios;
