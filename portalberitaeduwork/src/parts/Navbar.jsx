import React from "react";
import Box from "@mui/material/Box";
// import Container from "@mui/material/Container";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { makeStyles } from "@mui/styles";
import { Link } from "react-router-dom";
import Grid from "@mui/material/Grid";
import { grey, blue } from "@mui/material/colors";
import { useTheme } from "@mui/material";

const useStyles = makeStyles({
  myText: {
    color: "#FFF",
    textDecoration: "none",
    textTransform: "uppercase",
    paddingRight: 25,
    fontWeight: "bold",
    " &:hover": {
      color: blue[500],
      fontWeight: "bold",
    },
  },
  myHeadline: {
    color: "common.white",
    fontWeight: "bold",
    paddingLeft: 8,
  },
});

function Navbar() {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Box>
      <AppBar
        sx={{
          padding: "20px 25px",
          position: "flex",
          width: "100%",
          backgroundColor: "primary.main",
        }}
      >
        <Toolbar>
          <Grid container alignItems="center" spacing={1}>
            <Grid item md="auto" lg={2}>
              <Typography
                component="h1"
                variant="h6"
                className={classes.myHeadline}
              >
                PORTAL BERITA
              </Typography>
            </Grid>
            <Grid item md lg={3}>
              <Grid
                item
                md="auto"
                lg="auto"
                sx={{ width: "500px", bg: "yellow" }}
              >
                <Link to="/" className={classes.myText}>
                  Home
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Navbar;
