import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Navbar from "../parts/Navbar";
import Grid from "@mui/material/Grid";
import useFetchApi from "../customHook/FetchApi";
import Container from "@mui/material/Container";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import CardProducts from "../component/CardProducts";
import useDebounce from "../helpers/useDebounce";
import Card from "@mui/material/Card";
import { tokenHeader, url, publicHeader } from "../helpers/config.jsx";
import axios from "axios";
import FormControl from "@mui/material/FormControl";
// import { InfinitySpin } from "react-loader-spinner";
import LoadingOverlay from "react-loading-overlay";
import { Radio } from "react-loader-spinner";
import { convertLength } from "@mui/material/styles/cssUtils";
import useAxios from "../customHook/useAxios";

function Home() {
  let [query, setQuery] = useState("");
  // let [kondision, setkondision] = useState(true);
  let lastValue = useDebounce(query, 200);
  // let [tabelData, setTabelData] = useState([]);
  // let [isFetching, setIsFetching] = useState(true);
  let hdr = tokenHeader();
  //Array Desctruct Urutan nya harus sama seperti sebelumnya, kecuali Object Desctruct Urutannya tidak harus sama
  let {
    loading: newsLoading,
    data: newsData,
    error: newsError,
  } = useAxios({
    url: lastValue
      ? `${url.api}/v2/everything?q=${lastValue}`
      : `${url.api}/v2/top-headlines?country=id`,
    config: {
      method: "GET",
      headers: hdr,
      body: {},
    },
  });
  // useEffect(() => {
  //   sendRequest();
  //   console.log(query);
  //   // console.info(data);
  //   // handleData(data);
  // }, [query]);
  // console.log(data);
  // const [data, loading, error] = useFetchApi(
  //   "https://jsonplaceholder.typicode.com/users"
  // );
  // const hdr = tokenHeader();
  // const getData = async () => {
  //   // setIsFetching(true);
  //   await axios({
  //     method: "GET",
  //     url: `https://newsapi.org/v2/everything?q=${lastValue}`,
  //     headers: hdr,
  //     body: {},
  //   })
  //     .then((res) => {
  //       console.log(res.data.status);
  //       if (res.data.articles) {
  //         let data = res.data.articles;
  //         console.log("hasil response", data);
  //         setTabelData(data);
  //         setIsFetching(false);
  //       }
  //       console.log("data", tabelData);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //       setIsFetching(false);
  //     });
  // };
  // useEffect(() => {
  //   getData();
  // }, []);
  // if (loading) {
  //   return (
  //     <>
  //       <h1>Loading</h1>
  //     </>
  //   );
  // }
  // if (error) {
  //   console.log(error);
  // }
  // console.log("data", data);
  const handleData = (e) => {
    e.preventDefault();
    let q = e.target.search.value;
    console.log(q);
    alert("Behasil");
    if (q.length >= 3 || q.length <= 0) {
      setQuery(q);
      // request(q);
      // getData(q);
    }
    // if (q.lenght >= 2) {
    //   loading(false);
    // } else {
    //   loading(true);
    // }
    console.log("keyword", q);
  };
  return (
    <>
      <Box>
        <Navbar />
        <Box
          sx={{ backgroundColor: "background.paper", color: "common.black" }}
        >
          <Container maxWidth="xl" sx={{ pt: 20, pb: 12 }}>
            <Grid container alignItems="center">
              <Grid item xs>
                <form onSubmit={handleData}>
                  <Box sx={{ mx: 4, my: 2 }}>
                    <input type="text" id="search" />
                  </Box>
                  <Box sx={{ mx: 4, my: 2 }}>
                    <Button
                      type="submit"
                      variant="contained"
                      size="large"
                      color="primary"
                      sx={{
                        height: "100%",
                        borderRadius: "2px",
                        marginLeft: "12px",
                      }}
                    >
                      Search
                    </Button>
                  </Box>
                </form>
                <Card
                  style={{
                    margin: "auto",
                  }}
                >
                  <Radio
                    visible={newsLoading}
                    height="80"
                    width="80"
                    ariaLabel="radio-loading"
                    wrapperStyle={{}}
                    wrapperClass="radio-wrapper"
                  />
                  {newsData &&
                    newsData?.map((value) => {
                      return (
                        <div>
                          <CardProducts
                            alt={value.author}
                            image={value.urlToImage}
                            title={value.title}
                            desc={value.description}
                          />
                        </div>
                      );
                    })}
                </Card>
              </Grid>
            </Grid>
          </Container>
        </Box>
      </Box>
    </>
  );
}

export default Home;
