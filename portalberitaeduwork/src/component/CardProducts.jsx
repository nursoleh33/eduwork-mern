import React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import CardActionArea from "@mui/material/CardActionArea";
// import ProductsSatu from "../assets/image/Productsatu.png";
// import ProductsDua from "../assets/image/productsdua.png";
// import ProductsTiga from "../assets/image/productsempat.png";

const CardProducts = ({ title, desc, image, alt }) => {
  return (
    <Box>
      <Container maxWidth="lg">
        <Grid
          container
          alignItems="center"
          sx={{ display: "flex", justifyContent: "center", marginBottom: 5 }}
        >
          <Grid item xs={6} lg={4} md={4}>
            <Card
              sx={{ maxWidth: 250, marginTop: 4, height: "auto" }}
            >
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="100%"
                  image={image}
                  alt={alt}
                />
                <CardContent>
                  <Typography
                    variant="p"
                    component="h3"
                    align="center"
                    gutterBottom
                  >
                    {title}
                  </Typography>
                  <Typography variant="p">{desc}</Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
          <Grid item xs={6} lg={4} md={4}>
            <Card sx={{ maxWidth: 250, marginTop: 4, height: "auto" }}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="100%"
                  image={image}
                  alt="bola"
                />
                <CardContent>
                  <Typography
                    variant="p"
                    component="h3"
                    align="center"
                    gutterBottom
                  >
                    {title}
                  </Typography>
                  <Typography variant="p">{desc}</Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
          <Grid item xs={6} lg={4} md={4}>
            <Card sx={{ maxWidth: 250, marginTop: 4, height: "auto" }}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="100%"
                  image={image}
                  alt="bola"
                />
                <CardContent>
                  <Typography
                    variant="p"
                    component="h3"
                    align="center"
                    gutterBottom
                  >
                    {title}
                  </Typography>
                  <Typography variant="p">{desc}</Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default CardProducts;
