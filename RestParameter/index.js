//Spred Operator yang digunakan untuk memecah item dari sebuah array, object, atau string
//Destructuring Assigements Fitur untuk memecah item dari array / Object ke dalam variabel yang berbeda (memecah sekaligus memasukan dalam variabel)

//Kapan di butuhkan : Ketika memanggil suatu komponent di react js dan ketika mengirimkan sebuah parameter yang memiliki paramternya banyak yang dapat di persingkat dengan spred operator
//beberapa type data yang tidak boleh di gunakan spered operator adalah bolean, yang boleh di berikan spreed operator nilai iterabel contoh string, array dan object 

const str = "Belajar Mern";
const arr = [1,2,3,4,5];
const obj = {
    nama : "edi",
    umur : 23
};
console.log(...str) //hasilnya di pecah ada komanya
console.log(...arr) //hasilnya akan keluar dari array
console.log({...obj}) //value objetnya ga bisa di keluarin

//Contoh pengunan spered operaator sebagai parameter
function hitung(a,b,c,d,e){
    return a + b + c + d + e

}
const hasil = hitung(...arr)
console.log(hasil)

//Mengabungkan/concatination array atau object 
const arr2 = [6,7,8];
const arrCon = [...arr, ...arr2];
//cara lama
// const arrCon = arr.concat(arr2)
console.log(arrCon)

//concatination Object
const profile = {
    hobi: ["noding", "makan", "tidur"]
}
const objCon = {...obj, ...profile};
console.log(objCon)

//Menggunakan Destructuring relatif terhadap index
//jika ingin megabil value dari semua array kita destructuringnya juga perlu menggunakan array
const colors= ["Merah", "Kuning", "Hijau"];
let [, kuning] = colors;
console.log(kuning)

//Menggunakan Destructuringg Acesment
const profiles = {
    name : "Edi Hartono",
    umur: 23
};
//kalau kita mau peach sebagai object, kita tampung sebagai object

const {umur} = profiles;
// console.log(name, umur)
//jadi kalau bject ingin mengambil umurnya tidak perlu skip.
console.log(umur)

//Rest Operator (Merupakan Kombinasi Spread Operator dengan Destructuring Assigment)
const users =[
    {id: 1, name: "edi"},
    {id: 2, name: "dodi"},
    {id: 3, name: "bayu"},
    {id: 4, name: "rahma"},
    {id: 5, name: "aji"}
];
const [edi, ...lainnya] = users;
console.log(edi) //object pertama yang di ambil
console.log(lainnya)//mulai object kedua sampai akhir yang di ambil