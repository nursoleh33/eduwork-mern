// Task 1 --> Percabangan

var nilaiRaport = prompt("Input Nilai");
if(nilaiRaport >= 80 && nilaiRaport <=100){
    alert('Nilai Raport Anda A')
}else if(nilaiRaport >= 60 && nilaiRaport <=75){
    alert("Nilai Raport Anda B")
}else if(nilaiRaport >= 40 && nilaiRaport <=50){
    alert("Nilai Raport C")
}else{
    alert("Nilai Raport Anda D")
}
// For OF untuk Looping Object / Array
let keluarga = [
    {
        nama: 'joko',
        status: 'bekerja',
        pendapatan: 3000000
    },
    {
        nama: 'siti',
        status: 'bekerja',
        pendapatan: 2000000
    },
    {
        nama: 'wahyu',
        status: 'mahasiswa',
        pendapatan: 500000
    }
]
// Looping menggunakan ForEach untuk mengubah sesuatu contoh nama ubah menjadi capital
let kk = keluarga.forEach(function(kk){
    // console.log(kk.nama)
    return kk;
})
// console.log(kk)
//Lopping menggunakan Map
let kk1 = keluarga.map(function(kk){
    //bisa dpat di manupasi isi nilai nya
    // return kk
    // return {
    //     nama : kk.nama.toLocaleUpperCase(),
    //     status : kk.status,
    //     pendapatan : kk.pendapatan
    // }
    //tetapi terdapat undefined jumlah array masih sama 
    if(kk.status === "bekerja"){
        return kk
    }
})
console.log(kk1)
//Looping menggunakn Filter
let kk2 = keluarga.filter(function (kk){
    //bisa di lakukan untuk filter status bekerja
    // return kk
    //jika nilai tidak terpenuhi maka array akan hilang
    if(kk.status === "bekerja"){
        return kk
    }
})
console.log(kk2)

//Looping menggunakan Reduce, ada beberapa parameter yag harus di penuhi prev(nilai awal/sebelumnya) curr(nilai sekarang)
let kk3 = keluarga.reduce(function(prev, curr){
    //biasanya di guanakan untuk menjumlahkan 
    // return curr data yang index 0
    //return prev //data index
    return prev + curr.pendapatan
    
}, 0)
console.log(kk3)
//menghitung pendapatan yang berkaja
let kkBaru = keluarga.map(kk =>{
    return {
        nama : kk.nama.toUpperCase(),
        status : kk.status,
        pendapatan : kk.pendapatan
    }
}).filter(kk => kk.status === "bekerja")
.reduce((prev, curr) => prev + curr.pendapatan,0)
console.log(kkBaru)