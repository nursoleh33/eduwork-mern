// Fungsi/ Method yang ada di dalam Object
//class Merupakan pembungkus dari semua method
class Orang {
   constructor(name){
    this.name = name;
    console.log('Hari ini Ngoding')
   }
    makan() {
        console.log("sedang makan")
    }
    static jalan() {
        console.log('sedang jalan')
    }
}
//Object Orang yang memiliki variabel edi

const edi = new Orang();
//Inherit/Class Turunan
class Person {
    constructor(nama){
        this.nama = nama
    }
}
const soleh = new Person("Soleh");  
console.log(soleh.nama);


//Property yang ada di dalam Object, karena javascript mendukung pembuatan object secara literal.
edi.umur = 23;
// Alternatif cara kedua membuat method yang ada d dalam object
edi.tidur = function () {
    console.log("Sedang Tidur")
}
console.log(edi.umur)
// console.log(edi.makan())
edi.makan()
console.log(edi.tidur())

//Static method merupakan fungsi yang menempel pada class, bisa digunakan tanpa membuat object akan jalan berdasarkan Class bukan Object
Orang.jalan() //langsung panggil class Orang tanpa harus membuat object
// constructor sebuah fungsi yang di jalankan ketika object di inisialisasi dan dijalankan otomatis ketika object itu di buat
//dan constructor akan di panggil ketika pertama kali
//constructor tidak bisa di panggil dari luar
// edi.constructor()

// contoh lain inherit
//extends merupakan class turunan/inherit
class Pekerjaan extends Orang {
    //paramter name mengacu pada name yang ada di class Orang
    constructor(name){
        //menggunakan sifat yang sama seperti class Orang
        super(name);
    }
}
const programmer = new Pekerjaan("edi");
console.log(programmer)
console.log(programmer.name);
//programmer juga bisa akses method yang ada di class Orang
console.log(programmer.makan())