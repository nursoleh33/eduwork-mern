import hello, {word} from "./module.js"; //bisa menggunakan extension js atau tdk, jika menggunakn export default untuk pemanggilan nya harus seperti ini tanpa braket
import { salam, jalan } from './module.js'; //jika pemanggilan nya bukan secara export default maka harus menggunakn bracket karena dapat digunakan lebih dari 1 method dengan tanda pemisah koma.
import {tabel} from "./tabels.js";
// console.log(tabel())
// console.log(Table())
const app = document.getElementById('App');
tabel.render(app)
// console.log(app)
hello();
console.log(salam())
console.log(jalan())
word();
// console.log(jalan())
