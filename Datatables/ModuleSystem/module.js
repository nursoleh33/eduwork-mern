//Module System  -> cara kita untuk mengisolasikan kode dari 1 file ke file lain merupakan Feat Terbaru.
//import digunakan untuk mengambil variabel atau menthod
//export mengeluarkan method yang ingin digunakan di file lain
const hello = () =>console.log("Hello");
const word = () => console.log("word");
export default hello; //export default hanya bisa digunakan 1x dalam 1 file.
export { word }
// cara kedua
function jalan(){
    return "selamat jalan"
}
function salam(){
    return "selamat datang"
}
export {salam, jalan}; //cara eksport seperti ini memiliki banyak fungsi