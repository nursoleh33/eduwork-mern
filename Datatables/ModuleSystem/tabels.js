class Table {
    constructor(init){
        this.init = init;
    }
    createHeader(data){
        let open = "<thead><tr>";
        let close = "</tr></thead>";
        data.map((d, index)=>{
            open += `<th key=${index}>${d}</th>`
        })
        return open + close;
    }
    createBody(data){
        let open = "<tbody>";
        let close = "</body>";
        data.map((d, index)=>{
            open += `
            <tr key=${index}>
                <td>${d[0]}</td>
                <td>${d[1]}</td>
            </tr>
            `;
        })
        return open + close;
    }
    render(element){
        let tabel = `<table class="table table-success table-striped bg-danger">
        ${this.createHeader(this.init.columns)}
        ${this.createBody(this.init.data)}
        </table>`;
        element.innerHTML= tabel;
    }
}
const tabel = new Table({
    columns : ['Nama', 'Alamat Email'],
    data : [
        ["Edi Hartono", "edi-eduwork@gmail.com"],
        ["dodi Prakoso", "dodi@gamil.com"],
        ["Nur Muhamad Soleh", "nursoleh33@gmail.com"]
    ]
});
export {tabel};
// const app = document.getElementById('App');
// //method render untuk memasukan kedalam element yang sudah di tandai yaitu app
// tabel.render(app)
