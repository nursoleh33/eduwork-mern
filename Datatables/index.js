class Table {
    constructor(init){
        this.init = init;
    }
    createHeader(data){
        let open = "<thead><tr>";
        let close = "</tr></thead>";
        data.map((d, index)=>{
            open += `<th key=${index}>${d}</th>`
        })
        return open + close;
    }
    createBody(data){
        let open = "<tbody>";
        let close = "</body>";
        data.map((d, index)=>{
            open += `
            <tr key=${index}>
                <td>${d[0]}</td>
                <td>${d[1]}</td>
            </tr>
            `;
            console.log(data)
        })
        return open + close;
    }
    render(element){
        let tabel = `<table class="table table-success table-striped">
        ${this.createHeader(this.init.columns)}
        ${this.createBody(this.init.data)}
        </table>`;
        element.innerHTML= tabel;
        console.log(element.innerHTML)
    }
}
const tabel = new Table({
    columns : ['name', 'email'],
    data : [
        ["Edi Hartono", "edi@eduwork@gmail.com"],
        ["dodi Prakoso", "dodi@gamil.com"],
        ["Nur Muhamad Soleh", "nursoleh33@gmail.com"]
    ]
});
console.log(tabel)
const app = document.getElementById('app');
//method render untuk memasukan kedalam element yang sudah di tandai yaitu app
tabel.render(app)
