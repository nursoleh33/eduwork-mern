import Input from "../../components/Input";
import React, { useState, useEffect } from "react";
import "./index.scss";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";

const Tambah = () => {
  const nav = useNavigate();
  let [loading, setLoading] = useState(false);
  let [base46Image, setBase64Image] = useState("https://fakeimg.pl/350x200/");
  // Handle Change Input
  // const handleImage = (e) => {
  //   //tersimpan di tempory folder
  //   console.log(e.target.files[0]);
  //   // membuat tempory folder
  //   let uploaded = e.target.files[0];
  //   //menagmbil Temporary Folder dan mengubah menjadi url
  //   // console.log("url", URL.createObjectURL(uploaded));
  //   setImage(URL.createObjectURL(uploaded));
  //   //untuk menyimpan perumahan yang ada di input dan langsung dkirim ke backend
  //   setSaveImage(uploaded);
  // };

  let [saveImage, setSaveImage] = useState(null);
  const iniState = {
    name: "",
    price: "",
    stock: "",
    status: "",
    image: "",
  };

  const [initialValues, setInitialValue] = useState(iniState);
  const validationSchema = yup
    .object({
      name: yup
        .string()
        .required("Field Nama Wajib Di isi")
        .min(5, "Minimal 5 Karekter"),
      price: yup
        .number("Harus Number")
        .positive("Positif Number")
        .integer("Harus Interger")
        .required("Price Wajib di isi"),
      stock: yup.number().positive().integer().required("Stock Wajib di isi"),
      status: yup.boolean().oneOf([true, false]),
      image: yup
        .mixed()
        .test("required", "Anda perlu Upload File", (file) => {
          //File Tidak boleh kosong pada saat di Upload

          // return file && file.size <-- u can use this if you don't want to allow empty files to be uploaded;
          // if (file[0]?.length > 0 || "should be greater than 0") {
          //   return true;
          // }
          // return false;
          if (file[0]) return true;
          return false;
        })
        .test("fileSize", "Ukuran Image Terlalu Besar", (file) => {
          console.log(file[0]);
          //if u want to allow only certain file sizes
          return file && file[0]?.size <= 2000000;
        })
        .test("fileType", "Unsupported File Format", (value) =>
          ["image/jpeg", "image/png", "image/jpg"].includes(value[0]?.type)
        ),
    })
    .required();
  const {
    register,
    watch,
    reset,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onTouched",
    reValidateMode: "onChange",
    defaultValues: initialValues,
    resolver: yupResolver(validationSchema),
  });
  const handleImage = (e) => {
    console.log(e.target.files[0]);
    let uploaded = e.target.files[0];
    console.log("url", URL.createObjectURL(uploaded));
    setBase64Image(URL.createObjectURL(uploaded)); //image yang di ingin di tampilkan di FE perlu di convert
    console.log("uploaded", uploaded);
    setSaveImage(uploaded); //ini yang dikirim ke BE
    console.log(saveImage);
  };
  const handleAdd = (data) => {
    let done = true;
    // let post = data;
    console.log(data);
    // let targetImage = data.picture[0];
    // console.log(image);
    // console.log("url", URL.createObjectURL(image));
    // setBase64Image(URL.createObjectURL(targetImage));
    // console.log("base64", base46Image);
    // setSaveImage(targetImage);
    // console.log("saveImage", saveImage);
    let formData = new FormData();
    formData.append("name", data?.name);
    formData.append("price", data?.price);
    formData.append("stock", data?.stock);
    formData.append("status", data?.status);
    formData.append("image", saveImage);
    if (done) {
      Swal.fire({
        title: "Apa anda yakin?",
        text: "Data akan tersimpan di database",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, Simpan data!",
      }).then((result) => {
        if (result.isConfirmed) {
          setLoading(true);
          // let data = [];
          axios({
            method: "POST",
            url: "http://localhost:8000/api/v2/product",
            data: formData,
          })
            .then((res) => {
              console.log(res);
              if (res.status === 200) {
                let data = res.data;
                setValue("status", data?.status);
                Swal.fire(
                  "Berhasil!",
                  "Data Product Berhasil Di Tambhakan.",
                  "success"
                );
                // window.location.href = res.image
              }
              // data.push({
              //   name:
              // })
              setLoading(false);
              reset();
              nav(`/`);
            })
            .catch((err) => {
              console.log(err);
              setLoading(false);
              Swal.fire(
                "Peringatan!",
                "Terjadi kesalahan saat menambah data Product",
                "warning"
              );
            });
        }
      });
    }
  };
  return (
    <div className="main">
      <div className="card">
        <h2>Tambah Produk</h2>
        <br />
        <form onSubmit={handleSubmit(handleAdd)}>
          <input
            name="name"
            type="text"
            placeholder="Nama Produk..."
            label="Nama"
            {...register("name")}
          />
          {errors.name && (
            <p style={{ color: "red", fontWeight: "bold" }}>
              {errors.name.message}
            </p>
          )}
          <input
            name="price"
            type="number"
            placeholder="Harga Produk..."
            label="Harga"
            {...register("price")}
          />
          {errors.price && (
            <p style={{ color: "red", fontWeight: "bold" }}>
              {errors.price.message}
            </p>
          )}
          <input
            name="Stock"
            type="number"
            placeholder="Stock Produk..."
            label="Stock"
            {...register("stock")}
          />
          {errors.stock && (
            <p style={{ color: "red", fontWeight: "bold" }}>
              {errors.stock.message}
            </p>
          )}
          <input
            name="status"
            type="checkbox"
            label="Active"
            {...register("status")}
          />
          {errors.status && (
            <p style={{ color: "red", fontWeight: "bold" }}>
              {errors.status.message}
            </p>
          )}
          {/* <input type="file" {...register("picture")} name="picture" />
          {errors.picture && (
            <p style={{ color: "red", fontWeight: "bold" }}>
              {errors.picture.message}
            </p>
          )} */}
          <div>
            <img src={base46Image} className="img-thumbnail" alt="" />
            <label htmlFor="formfile" className="form-label">
              {base46Image === "https://fakeimg.pl/350x200/"
                ? "Upload Image Here"
                : ""}
            </label>
            <input
              type="file"
              className="form-control"
              style={{ marginTop: 25 }}
              id="formfile"
              accept="image/*"
              {...register("image")}
              onChange={(e) => handleImage(e)}
            />
            {errors.image && (
              <p style={{ color: "red", fontWeight: "bold" }}>
                {errors.image.message}
              </p>
            )}
          </div>
          <button type="submit" className="btn btn-primary">
            Simpan
          </button>
        </form>
      </div>
    </div>
  );
};

export default Tambah;
