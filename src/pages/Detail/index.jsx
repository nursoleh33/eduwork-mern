// import { Link } from "react-router-dom";
import axios from "axios";
import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import "./index.scss";
import Swal from "sweetalert2";

const Detail = () => {
  const { id } = useParams();
  console.log("detail_id", id.id);
  let [detail, setDetail] = useState({
    id: "",
    name: "",
    price: "",
    stock: "",
  });
  let [loading, setLoading] = useState(false);

  const getData = async () => {
    setLoading(true);
    await axios({
      url: `http://localhost:8000/api/v2/product/${id}`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      data: {},
    })
      .then((res) => {
        console.log(res);
        if (res.status === 200) {
          let data = res.data;
          console.log(data);
          setDetail({
            id: data._id,
            name: data.name,
            price: data.price,
            stock: data.stock,
          });
          console.log(data);
        }
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        Swal.fire("Sorry", "Data Gagal Di Tampilkan", "error");
        setLoading(false);
      });
  };
  useEffect(() => {
    getData();
  }, []);
  // useEffect(() => {
  //   console.log(detail);
  // }, [detail]);
  console.log("price", detail?.price);
  return (
    <div className="main">
      <Link to="/" className="btn btn-primary">
        Kembali
      </Link>

      <table className="table">
        <tbody>
          <tr>
            <td>ID</td>
            <td>{detail.id}</td>
          </tr>
          <tr>
            <td>Name</td>
            <td>{detail.name}</td>
          </tr>
          <tr>
            <td>Price</td>
            <td>{detail.price}</td>
          </tr>
          <tr>
            <td>Stock</td>
            <td>{detail.stock}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Detail;
