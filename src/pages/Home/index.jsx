import React, { useState, useEffect, useMemo } from "react";
import { useNavigate, Link } from "react-router-dom";
import "./index.scss";
import axios from "axios";
import Swal from "sweetalert2";

const Home = () => {
  const nav = useNavigate();
  const [product, setProduct] = useState([]);
  let [filter, setFilter] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchValue, setSearchValue] = useState("");

  const getData = async () => {
    setLoading(true);
    await axios({
      url: "http://localhost:8000/api/v2/product",
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      data: {},
    })
      .then((res) => {
        // console.log(res);
        if (res.status === 200) {
          let data = res.data;
          setProduct(data);
          //
          setFilter(data);
          console.log(data);
          // console.log(data);
        }
        setLoading(false);
      })
      .catch((err) => {
        // console.log(err);
        Swal.fire("Sorry", "Data Gagal Di Tampilkan", "error");
        setLoading(false);
      });
  };
  useEffect(() => {
    getData();
  }, []);
  useEffect(() => {
    // getData();
    const newPacientes = product.filter((value) => {
      console.log("value name", value.name);
      console.log("search value", searchValue);
      return value?.name.toLowerCase().includes(searchValue.toLowerCase());
    });
    // setProduct(newPaciente);
    setFilter(newPacientes);
  }, [searchValue]);
  // useEffect(()=>{

  // },[])
  const handleDetail = (id) => {
    // console.log("id", id);
    window.location.href = `/detail/${id}`;
  };
  const handleEdit = (id) => {
    // console.log("id", id);
    window.location.href = `/edit/${id}`;
  };
  const handleDelete = (id) => {
    // console.log("id", id);
    Swal.fire({
      title: "Apa anda yakin?",
      text: "Apakah ada yakin ingin menghapus data ini!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        setLoading(true);
        axios({
          method: "DELETE",
          url: `http://localhost:8000/api/v2/product/${id}`,
          headers: {
            "Content-Type": "application/json",
          },
          data: {},
        })
          .then((res) => {
            // console.group(res);
            Swal.fire("Berhasil!", "Data berhasil di hapus.", "success");
            getData();
            setLoading(false);
          })
          .catch((err) => {
            // console.log(err);
            Swal.fire(
              "Peringatan!",
              "Terjadi kesalahan saat menghapus data",
              "warning"
            );
            setLoading(false);
          });
      }
    });
  };
  // console.log("product", product);
  // console.log("seacrc", search.target.value);
  // const handleFilter = (value) => {
  //   setSearch(value);
  //   console.log(value);

  //   setProduct(search);
  // };
  // const productData = useMemo(() => {
  //   let products = product;
  //   if (search) {
  //     products.filter((product) =>
  //       product.name.toLowerCase().includes(search.toLowerCase())
  //     );
  //   }
  // }, [product, search]);
  // const commentData = useMemo(() => {
  //   let computedCommets = product;
  //   let dataSearch = search;
  //   console.log(dataSearch);
  //   if (dataSearch) {
  //     computedCommets.filter((comment) => {
  //       comment.name.toLowerCase().includes(dataSearch.toLowerCase());
  //     });
  //   }
  // });

  return (
    <div className="main">
      <Link to="/tambah" className="btn btn-primary">
        Tamah Produk
      </Link>
      <div className="search">
        <input
          type="text"
          placeholder="Masukan kata kunci..."
          onChange={(e) => setSearchValue(e.target.value)}
          value={searchValue}
        />
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th className="text-right">Price</th>
            <th className="text-center">Status</th>
            <th className="text-center">Image</th>
            <th className="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          {filter.map((item, index) => {
            // console.log(item);
            return (
              <tr>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td className="text-right">{item.price}</td>
                <td>{item.status ? "Tersedia" : "Kosong"}</td>
                <td>
                  <img src={item.image_url} alt={item.name} />
                </td>
                <td className="text-center">
                  <Link
                    className="btn btn-sm btn-info"
                    onClick={() => handleDetail(item._id)}
                  >
                    Detail Product
                  </Link>
                  <Link
                    className="btn btn-sm btn-danger"
                    onClick={() => handleDelete(item._id)}
                  >
                    Delete
                  </Link>
                  <Link
                    className="btn btn-sm btn-danger"
                    onClick={() => handleEdit(item._id)}
                  >
                    Edit
                  </Link>
                </td>
              </tr>
            );
          })}
          {/* <tr>
            <td>1</td>
            <td>Laptop</td>
            <td className="text-right">RP. 20.000.000</td>
            <td className="text-center">
              <Link to="/detail" className="btn btn-sm btn-info">
                Detail
              </Link>
              <Link to="/edit" className="btn btn-sm btn-warning">
                Edit
              </Link>
              <Link to="#" className="btn btn-sm btn-danger">
                Delete
              </Link>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>Monitor</td>
            <td className="text-right">RP. 10.000.000</td>
            <td className="text-center">
              <Link to="/detail" className="btn btn-sm btn-info">
                Detail
              </Link>
              <Link to="/edit" className="btn btn-sm btn-warning">
                Edit
              </Link>
              <Link to="#" className="btn btn-sm btn-danger">
                Delete
              </Link>
            </td>
          </tr> */}
        </tbody>
      </table>
    </div>
  );
};

export default Home;
