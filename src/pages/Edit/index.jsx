import Input from "../../components/Input";
import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

const Edit = () => {
  let { id } = useParams();
  console.log(id);
  const nav = useNavigate();
  let [loading, setLoading] = useState(false);
  let [base46Image, setBase64Image] = useState("https://fakeimg.pl/350x200/");
  let [preview, setPreview] = useState(null);
  // let [saveImage, setSaveImage] = useState(null);
  const iniState = {
    name: "",
    price: "",
    stock: "",
    status: "",
    image: "",
  };
  const [initialValues, setInitialValue] = useState(iniState);
  const validationSchema = yup
    .object({
      name: yup
        .string()
        .required("Field Nama Wajib Di isi")
        .min(5, "Minimal 5 Karekter"),
      price: yup.number().positive().integer().required("Price Wajib di isi"),
      stock: yup.number().positive().integer().required("Stock Wajib di isi"),
      status: yup.boolean().oneOf([true, false]),
      image: yup
        .mixed()
        .test("required", "Anda perlu Upload File", (file) => {
          //File Tidak boleh kosong pada saat di Upload
          // return file && file.size <-- u can use this if you don't want to allow empty files to be uploaded;
          // if (file[0]?.length > 0 || "should be greater than 0") {
          //   return true;
          // }
          // return false;
          if (file[0]) return true;
          return false;
        })
        .test("fileSize", "Ukuran Image Terlalu Besar", (file) => {
          console.log(file[0]);
          //if u want to allow only certain file sizes
          return file && file[0]?.size <= 2000000;
        })
        .test("fileType", "Unsupported File Format", (value) =>
          ["image/jpeg", "image/png", "image/jpg"].includes(value[0]?.type)
        ),
    })
    .required();
  const {
    register,
    watch,
    reset,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onTouched",
    reValidateMode: "onChange",
    defaultValues: initialValues,
    resolver: yupResolver(validationSchema),
  });
  const getDataProduct = async () => {
    setLoading(true);
    await axios({
      method: "GET",
      url: `http://localhost:8000/api/v2/product/${id}`,
      headers: {
        "Content-Type": "application/json",
      },
      data: {},
    })
      .then((res) => {
        console.log(res);
        if (res.status === 200) {
          let data = res.data;
          console.log(data);
          setValue("name", data?.name);
          setValue("price", data?.price);
          setValue("stock", data?.stock);
          setValue("status", data?.status);
          // setBase64Image(data.image);
          setValue("image_url", data?.image_url); //image_url untuk mendapatkan value dari upload file ynag akan dikirim
          // console.log("res url", URL.createObjectURL(data?.image_url));
          Swal.fire("sucess", "Data Berhasil Di Tampilkan", "success");
        }
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
        Swal.fire("Sorry", "Data Gagal Di Tampilkan", "warning");
      });
  };
  console.log("image", base46Image);
  const handleInputData = (data) => {
    let done = true;
    console.log("data", data);
    console.log(data.image[0]);
    const image = [...data.image];
    let uploaded = image[0];
    console.log("url", URL.createObjectURL(uploaded));
    // setSaveImage(uploaded);
    console.log(base46Image);
    console.log("data terikirim", data);
    let formData = new FormData();
    formData.append("name", data?.name);
    formData.append("price", data?.price);
    formData.append("stock", data?.stock);
    formData.append("status", data?.status);
    // jika ada file yang di Upload, maka kirim dalam bentuk URL ke BE
    if (uploaded) {
      formData.append("image", uploaded);
    }
    // setBase64Image(URL.createObjectURL(uploaded));
    console.log("form data", formData);
    console.log("form data Append", data.image[0]);
    console.log("base64", base46Image);
    // let post = data;
    if (done) {
      Swal.fire({
        title: "Apakah anda yakin ingin melakukan update?",
        text: "Data akan tersimpan di database",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, Simpan Mater Data",
      }).then((result) => {
        if (result.isConfirmed) {
          setLoading(true);
          axios({
            method: "PUT",
            url: `http://localhost:8000/api/v2/product/${id}`,
            data: formData,
          })
            .then((res) => {
              console.log(res);
              Swal.fire("Succes", "Data Berhasil di Simpan", "success");
              setLoading(false);
              reset();
              nav(`/`);
            })
            .catch((err) => {
              console.log(err);
              Swal.fire("Sorry", "Data Gagal Melakukan Simpan", "error");
              setLoading(false);
            });
        }
      });
    }
  };
  useEffect(() => {
    getDataProduct();
  }, []);
  // useEffect(()=>{
  // Clean UP Image Upload Yang sebelumya
  useEffect(() => {
    if (watch("image")[0]) {
      // create the preview
      const objectUrl = window.URL.createObjectURL(watch("image")[0]);
      setPreview(objectUrl);

      // cleanup
      return () => window.URL.revokeObjectURL(objectUrl);
    }
  }, [watch("image")]);
  // })
  console.log(watch("image"));
  return (
    <div className="main">
      <div className="card">
        <h2>Edit Produk</h2>
        <br />
        <form onSubmit={handleSubmit(handleInputData)}>
          <input
            name="name"
            type="text"
            placeholder="Nama Produk..."
            label="Nama"
            defaultValue={watch("name")}
            {...register("name")}
          />
          {errors.name && (
            <p className="fw-bold text-danger">{errors.name.message}</p>
          )}
          <input
            name="price"
            type="number"
            placeholder="Harga Produk..."
            label="Harga"
            defaultValue={watch("price")}
            {...register("price")}
          />
          {errors.price && (
            <p className="fw-bold text-danger">{errors.price.message}</p>
          )}
          <input
            name="Stock"
            type="number"
            placeholder="Stock Produk..."
            label="Stock"
            defaultValue={watch("stock")}
            {...register("stock")}
          />
          {errors.stock && (
            <p className="fw-bold text-danger">{errors.stock.message}</p>
          )}
          <input name="status" type="checkbox" {...register("status")} />
          {errors?.status && (
            <p className="fw-bold text-danger">{errors?.status.message}</p>
          )}
          <div>
            {console.log("base64", base46Image)}
            {loading ? (
              <p>Loading ....</p>
            ) : (
              <>
                {preview ? (
                  <img src={preview} alt="" />
                ) : (
                  <img src={watch("image_url")} alt="" />
                )}
                {/* <label htmlFor="formfile" className="form-label">
                  {base46Image === "https://fakeimg.pl/350x200/"
                    ? "Upload Image Here"
                    : ""}
                </label> */}
              </>
            )}
            <input
              type="file"
              className="form-control"
              style={{ marginTop: 25 }}
              id="formfile"
              accept="image/*"
              {...register("image")}
            />
            {errors.image && (
              <p style={{ color: "red", fontWeight: "bold" }}>
                {errors.image.message}
              </p>
            )}
          </div>
          <button type="submit" className="btn btn-primary">
            Simpan
          </button>
        </form>
      </div>
    </div>
  );
};

export default Edit;
