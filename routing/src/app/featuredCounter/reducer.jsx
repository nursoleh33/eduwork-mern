import React from "react";
import * as counter from "./constants";
//valuenya di simpan dalam bentuk object sama seperti membuat state
let initialState = {
  count: 0,
};

//reducer di buat bebas tidak ada aturan nya dengan react js

const CounterReducer = (state = initialState, action) => {
  //Logic menggunakan switch
  //actions.jsx di tangkat oleh reducer ke component Counter
  switch (action.type) {
    case counter.INC:
      return {
        //tidak bisa melakukan action yang di bawahnya sebelum melakukan copy state nya

        ...state, //melakukan copy state yang sebelumnya lalu timpa dengan yang baru
        count: state.count + action.value, // membuat lebih dinamis lagi bisa menggunakan action.value yang dimana action nya akan memilki payload, bisa tambhan value langsung 3 atau 5, nanti action akan mempunyai payload selain type juga mengirimkan data
      };
    case counter.DEC:
      return {
        ...state,
        count: state.count - action.value,
      };
    //Memberikan nilai default
    default:
      return state;
  }
};
export default CounterReducer;
