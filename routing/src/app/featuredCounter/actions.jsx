// action merupakan sebuah fungsi biasa, yang di mana fungsinya mengembalikan object

//action Creators file yang berisi semua aksi yang kita pisahkan dari dispatch secara terpisah

//action Creator bekerja secara syncrounus karena hanya merupakan fungsi javascript biasa, bukan merupakan bagian dari react dan juga reduce

import * as counter from "./constants";
export const increment = (value) => {
  return {
    type: counter.INC,
    value: value, //value ngambil dari paramter
  };
};
export const decrement = (value) => {
  return {
    //typenya sudah di definisikan disini
    type: counter.DEC,
    //menerima data nya ajja
    value: value,
  };
};

//middelware yang ada di reduce thunk untuk menagan error async
export const decrementWitchCheckingAction = (value) => {
  return (dispatch, getState) => {
    if (getState().counter.count > 0) {
      dispatch(decrement(value));
    }
  };
};
