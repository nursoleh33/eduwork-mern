import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import { Routes, Route } from "react-router-dom";
// import From from "./component/From";
import EditFrom from "./component/EditFrom";
// import FromData from "./component/EditFrom";
// import AddMahasiswa from "./component/AddMahasiswa";
import { Nav } from "reactstrap";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<EditFrom />} />
        {/* <Route path="/" element={<AddMahasiswa />} /> */}
        <Route path="/nav" element={<Nav />} />
        <Route path="/editmahasiswa/:id" element={<EditFrom />} />
        {/* <Route path="/addMahasiswa" element={} /> */}
      </Routes>
    </>
  );
}

export default App;
