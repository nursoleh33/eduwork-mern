import React from "react";
import { Container, Button, Label, Row, Col, Card } from "reactstrap";
import { useState, useEffect } from "react";
// import { Form } from "react-router-dom";
// import Form from "react-bootstrap/Form";
import { useNavigate, useParams } from "react-router-dom";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";
import Swal from "sweetalert2";

const AddMahasiswa = () => {
  const [update, setUpdate] = useState({
    nama: "",
    desc: "",
    email: "",
    jenisKelamin: "",
    jurusan: "",
    pendidikan: "",
  });
  let pendidikan = [
    {
      label: "",
      value: "SD",
      // status: update.pendidikan === "SD" ? 'selected' : '',
    },
    {
      label: "",
      value: "SMP",
    },
    {
      label: "SMK",
      value: "SMK",
    },
    {
      label: "S1",
      value: "S1",
    },
  ];
  const EducationSelected = (event) => {
    let {
      target: { value = "", name = "" },
    } = event;
    setUpdate((prevState) => ({ ...prevState, [name]: value }));
  };
  const [mahasiswa, setMahasiswa] = useState([]);
  const [loading, setLoading] = useState(false);
  const getMahasiswa = async () => {
    setLoading(true);
    await axios({
      url: `http://localhost:3000/mahasiswa`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      body: {},
    })
      .then((res) => {
        consolelog(res);
        if (res.status == 200) {
          let data = res.data;
          setMahasiswa(data);
          setLoading(false);
        }
        Swal.fire("sucess", "Data Berhasil Di Tampilkan", "success");
      })
      .catch((err) => {
        console.log(err);
        Swal.fire("Erorr", "Terjadi kesalahan saat menampilkan data", "error");
        setLoading(false);
      });
  };
  const handleEdit = (id) => {
    nav(`/editmahasiswa/${id}`);
  };
  const deleteData = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, Delete Data!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          method: "DELETE",
          url: `http://localhost:3000/mahasiswa/${id}`,
          headers: {
            "Content-Type": "application/json",
          },
          data: {},
        })
          .then((res) => {
            console.log(res);
            Swal.fire(
              "Berhasil!",
              "Data kop surat berhasil di hapus.",
              "success"
            );
            getDataMahasiswa();
          })
          .catch((err) => {
            console.log(err);
            Swal.fire(
              "Peringatan!",
              "Terjadi kesalahan saat menghapus data",
              "warning"
            );
          });
      }
    });
  };
  const handleInputData = () => {
    let done = true;
    let dataNew = {
      nama: update.nama,
      email: update.email,
      desc: update.desc,
    };
    if (done) {
      Swal.fire({
        title: "Apakah anda yakin ingin melakukan update?",
        text: "Data akan tersimpan di database",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, Simpan Data Mahasiswa",
      }).then((result) => {
        setLoading(true);
        axios({
          url: `http://localhost:3000/mahasiswa`,
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          data: JSON.stringify(dataNew),
        })
          .then((res) => {
            getMahasiswa(res.data.id);
            setLoading(false);
            setUpdate({
              nama: "",
              email: "",
              desc: "",
              id: 0,
            });
            Swal.fire("Sukses", "Data mahasiswa berhasil di input", "success");
          })
          .catch((err) => {
            console.log(err);
            setLoading(false);
          });
      });
    }
  };
  const nav = useNavigate();
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  return (
    <>
      {loading ? (
        <h1>Proses Loading</h1>
      ) : (
        <div>
          <Container>
            <form type="post" onSubmit={handleSubmit(handleInputData)}>
              <Card className="py-4 my-3">
                <h5>Biodata Mahasiswa</h5>
                <hr />
                <Row className="mt-3">
                  <Col xs="12" lg="4">
                    <Label>Nama Lengkap</Label>
                    <input
                      type="text"
                      placeholder="Masukan Nama Anda"
                      onChange={(t) => {
                        setUpdate({ ...update, nama: t.target.value });
                      }}
                      {...register("nama", {
                        required: true,
                        maxLength: 20,
                        pattern: /^[A-Za-z]+$/i,
                      })}
                    />
                    {errors?.nama?.type === "required" && (
                      <p>Field Nama wajib di isi</p>
                    )}
                    {errors?.nama?.type === "maxLength" && (
                      <p>Maximal Karakter yang di input 20 Karakter</p>
                    )}
                    {errors?.nama?.type === "pattern" && <p>Harus Alfabet</p>}
                  </Col>
                  <Col xs="12" lg="4">
                    <Label>Alamat Email</Label>
                    <input
                      type="email"
                      placeholder="Masukan Nama Anda"
                      onChange={(t) => {
                        setUpdate({ ...update, email: t.target.value });
                      }}
                      {...register("email", {
                        required: true,
                        pattern: {
                          value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                          message: "invalid email address",
                        },
                      })}
                    />
                    {errors.email && <p>{errors.email.message}</p>}
                  </Col>
                  <Col xs="12" lg="4">
                    <Label>Deskripsi</Label>
                    <input
                      type="text"
                      placeholder="Masukan Pesan Anda"
                      onChange={(t) => {
                        setUpdate({ ...update, desc: t.target.value });
                      }}
                      {...register("desc", {
                        required: true,
                        maxLength: 10,
                      })}
                    />
                    {errors.desc && <p>{errors.desc.message}</p>}
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" lg="4">
                    <div>
                      <Label>Jenis Kelamin</Label>
                    </div>
                    <div className="d-flex justify-content-evenly w-100 align-items-center">
                      <input
                        id="gender"
                        name="gender"
                        type="radio"
                        value="l"
                        {...register("jenisKelamin", { required: true })}
                        onClick={() =>
                          setUpdate({ ...update, jenisKelamin: "l" })
                        }
                        className="m-0 p-0"
                      />
                      <Label>Laki - Laki</Label>
                      <input
                        id="gender"
                        name="gender"
                        type="radio"
                        value="p"
                        {...register("jenisKelamin", { required: true })}
                        onClick={() =>
                          setUpdate({ ...update, jenisKelamin: "p" })
                        }
                        className="m-0 p-0"
                      />
                      <Label>Perempuan</Label>
                    </div>
                  </Col>
                  <Col xs="12" lg="4">
                    <Label>Jurusan</Label>
                    <select
                      id="jurusan"
                      name="jurusan"
                      type="select"
                      value={""}
                      {...register("jurusan", { required: true })}
                    >
                      <option hidden>{""}</option>
                      <option value="Sistem Informasi">Sistem Informasi</option>
                      <option value="Ekonomi">Ekonomi</option>
                      <option value="MultiMedia">MultiMedia</option>
                      <option value="Sipil">Sipil</option>
                    </select>
                  </Col>
                  <Col xs="12" lg="4">
                    <Label>Pendidikan Terakhir</Label>
                    <select
                      type="select"
                      name="pendidikan"
                      onChange={EducationSelected}
                      className="form-select"
                    >
                      {pendidikan.map((val, index) => {
                        return (
                          <option key={index} value={val.value}>
                            {val.label}
                          </option>
                        );
                      })}
                    </select>
                  </Col>
                </Row>
              </Card>
              <div className="col-xs-12 col-lg-4 d-flex justify-content-between gap-3">
                <Button type="button" onClick={() => nav("/")}>
                  Cancel
                </Button>
                <Button type="submit">Simpan</Button>
              </div>
            </form>
            {mahasiswa.map((e) => {
              return (
                <div
                  key={e.id}
                  className="w-full p-4 bg-white text-gray-800 my-2 rounded-lg"
                >
                  <button
                    className="bg-red-500 text-gray-500 rounded-lg outline-none p-2 flex justify-center items-center ring-0"
                    onClick={() => deleteData(e.id)}
                  >
                    Delete
                  </button>
                  <button
                    className="bg-blue-500 text-gray-500 rounded-lg outline-none p-2 flex justify-center items-center ring-0 mt-4"
                    onClick={() => handleEdit(e.id)}
                  >
                    Update
                  </button>
                </div>
              );
            })}
          </Container>
        </div>
      )}
    </>
  );
};

export default AddMahasiswa;
