import axios from "axios";
import Swal from "sweetalert2";
import React, { useEffect, useState } from "react";
// import { Button, Container } from "react-bootstrap";
import {
  Form,
  Label,
  FormGroup,
  FormText,
  Container,
  InputGroup,
  FormFeedback,
  Button,
  Input,
} from "reactstrap";
import { useParams, useNavigate } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
// import Form from "react-bootstrap/Form";
import * as yup from "yup";
import { useForm } from "react-hook-form";

// const schema = yup.object({
//   nama: yup
//     .string()
//     .matches(/^[A-Za-a]+$/i, "alvabet")
//     .required("Nama is required")
//     .length(5, "must be at least 3 characters long")
//     .lowercase(),
//   email: yup.string().email(),
//   age: yup.number().positive().integer().required(), //angka yag di input harus number, dan harus angka positif, dan wajib di isi.
// });
//   .required();

const EditFrom = () => {
  const initState = {
    nama: "",
    email: "",
    desc: "",
    jenisKelamin: "",
    jurusan: "",
  };
  const [initialValues, setInitialValue] = useState(initState);
  const validationSchema = yup
    .object({
      email: yup.string().email().required("Wajib di isi fild email"),
      nama: yup
        .string()
        .matches(/^[A-Za-z ,.'-]+$/i, "Wajib Alfabet")
        .required("Fild Nama Wajib di isi")
        .min(5, "minimal 5 karakter")
        .lowercase("harus lowercase ya"),
      desc: yup
        .string()
        .min(5, "Minimal 5 karakter yang di input")
        .required("Wajib di isi fild deskripsi"),
      jenisKelamin: yup.string().required("Wajib Di ISI "),
      jurusan: yup.string().required("wajib Di isi Fild jurusan"),
    })
    .required();
  const handleSubmitData = (data) => {
    console.log(data);
  };
  const handleError = (error) => {
    console.log("ERORR:::");
  };
  const {
    register,
    watch,
    reset,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onTouched",
    reValidateMode: "onChange",
    defaultValues: initialValues,
    resolver: yupResolver(validationSchema),
  });
  //   const onSubmit = data => console.log(data);
  let { id } = useParams();
  const nav = useNavigate();
  const [update, setUpdate] = useState({
    nama: "",
    desc: "",
    email: "",
    jenisKelamin: "",
  });
  const [loading, setLoading] = useState(false);
  // let CheckStatus = update?.jenisKelamin ? "true" : "false";
  const getMahasiswa = async () => {
    setLoading(true);
    await axios({
      url: ` http://localhost:3000/mahasiswa`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      body: {},
    })
      .then((res) => {
        console.log(res);
        if (res.status == 200) {
          let data = res.data;
          //   console.log(typeof data);
          //   console.log(data.nama);
          setUpdate({
            nama: data.nama,
            email: data.email,
            desc: data.desc,
            jenisKelamin: data.jenisKelamin,
            // jenisKelamin: data.jenisKelamin,
          });
          setLoading(false);
        }
        Swal.fire("sucess", "Data Berhasil Di Tampilkan", "success");
      })
      .catch((err) => {
        console.log(err);
        nav("/");
        setLoading(false);
      });
  };
  const handleInputData = (data) => {
    let done = true;
    // let dataNew = {
    //   nama: update.nama,
    //   email: update.email,
    //   desc: update.desc,
    //   jenisKelamin: update.jenisKelamin,
    //   // jenisKelamin: update.jenisKelamin,
    //   // ...update,
    // };
    if (done) {
      Swal.fire({
        title: "Apakah anda yakin ingin melakukan update?",
        text: "Data akan tersimpan di database",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, Simpan Data Mahasiswa",
      }).then((result) => {
        if (result.isConfirmed) {
          setLoading(true);
          axios({
            method: "POST",
            url: `http://localhost:3000/mahasiswa`,
            headers: {
              "Content-Type": "application/json",
            },
            data: JSON.stringify(data),
          })
            .then((res) => {
              console.log(res);
              getMahasiswa(res.data.id);
              Swal.fire("Succes", "Data Berhasil di Simpan", "success");
              setLoading(false);
              reset();
              // setUpdate({
              //   nama: "",
              //   email: "",
              //   desc: "",

              // });
              nav("/");
            })
            .catch((err) => {
              console.log(err);
              setLoading(false);
              Swal.fire("Sorry", "Data Gagal Melakukan Simpan", "error");
            });
        }
      });
    }
  };
  useEffect(() => {
    getMahasiswa();
  }, []);
  return (
    <>
      {/* <h1>Soleh</h1> */}
      {loading ? (
        <h1>Proses Loading</h1>
      ) : (
        <Container>
          <Form onSubmit={handleSubmit(handleInputData)}>
            <FormGroup className="mb-3" controlId="formBasicNama">
              <Label>Nama Lengkap Anda</Label>
              <input
                type="text"
                placeholder="Masukan Nama Anda"
                // innerRef={register("nama")}
                // invalid={Boolean(errors?.nama)}
                {...register("nama")}
              />
              {/* <FormFeedback>Fild Nama Wajib Di isi</FormFeedback> */}
              {errors.nama && <p>{errors.nama.message}</p>}
            </FormGroup>
            <FormGroup className="mb-3" controlId="formBasicEmail">
              <Label>Email Anda</Label>
              <input
                type="email"
                placeholder="Masukan Email Anda"
                {...register("email")}
              />
              {/* <p>{errors?.email?.message}</p> */}
              {/* <FormFeedback valid={!Boolean(errors?.email)}>
                
              </FormFeedback> */}
              {errors.email && <p>{errors.email.message}</p>}
            </FormGroup>
            <FormGroup className="mb-3" controlId="formBasicDesc">
              <Label>Deskripsi</Label>
              <input
                type="text"
                placeholder="Masukan Message Anda"
                {...register("desc")}
              />
              {errors?.desc && <p>{errors?.desc.message}</p>}
            </FormGroup>
            <FormGroup>
              <div>
                <Label>Pilih Jurusan</Label>
              </div>
              <select
                id="jurusan"
                name="jurusan"
                type="select"
                // value={""}
                {...register("jurusan")}
              >
                <option hidden>{""}</option>
                <option value="Sistem Informasi">Sistem Informasi</option>
                <option value="Ekonomi">Ekonomi</option>
                <option value="MultiMedia">MultiMedia</option>
                <option value="Sipil">Sipil</option>
              </select>
              {errors?.jurusan && <p>{errors?.jurusan.message}</p>}
            </FormGroup>
            <FormGroup>
              <div>
                <Label>Jenis Kelamin</Label>
              </div>
              <div className="d-flex justify-content-evenly w-100 align-items-center">
                <input
                  id="gender"
                  name="gender"
                  type="radio"
                  value="l"
                  {...register("jenisKelamin")}
                  onClick={() => setUpdate({ ...update, jenisKelamin: "l" })}
                  className="m-0 p-0"
                />

                <Label>Laki - Laki</Label>
                <input
                  id="gender"
                  name="gender"
                  type="radio"
                  value="p"
                  {...register("jenisKelamin")}
                  onClick={() => setUpdate({ ...update, jenisKelamin: "p" })}
                  className="m-0 p-0"
                />
                <Label>Perempuan</Label>
                {errors?.jenisKelamin && <p>{errors?.jenisKelamin.message}</p>}
              </div>
            </FormGroup>
            <Button variant="danger">Cancel</Button>
            <Button variant="primary">Simpan</Button>
          </Form>
        </Container>
      )}
    </>
  );
};
export default EditFrom;
