import axios from "axios";
import Swal from "sweetalert2";
import React, { useEffect, useState, useRef } from "react";
import { Input, Label, Row, Col, Card, Button, FormGroup } from "reactstrap";
import { useParams, useNavigate } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";

const schema = yup.object({
  nama: yup
    .string()
    .matches(/^[A-Za-a]+$/i, "alvabet")
    .required("Nama is required")
    .length(5, "must be at least 3 characters long")
    .lowercase(),
  email: yup.string().email(),
  age: yup.number().positive().integer().required(), //angka yag di input harus number, dan harus angka positif, dan wajib di isi.
});
//   .required();

const EditFormData = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  //   const onSubmit = data => console.log(data);
  let { id } = useParams();
  const nav = useNavigate();
  const [update, setUpdate] = useState({
    nama: "",
    desc: "",
    email: "",
    jenisKelamin: "",
    jurusan: "",
    pendidikan: ""
  });
  let pendidikan = [
    {
      label: "",
      value: 'SD',
      // status: update.pendidikan === "SD" ? 'selected' : '',
    },
    {
      label: "",
      value: "SMP",
    },
    {
      label: "SMK",
      value: "SMK",
    },
    {
      label: "S1",
      value: "S1",
    },
  ]
  const EducationSelected = (event)=>{
    let {
      target: {value = "", name = ""},
    } = event;
    setUpdate((prevState)=>({ ...prevState, [name] : value}));
  };

  const [loading, setLoading] = useState(false);
  // let CheckStatus = update?.jenisKelamin ? "true" : "false";
  const getMahasiswa = async () => {
    setLoading(true);
    await axios({
      url: ` http://localhost:3000/mahasiswa/${id}`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      body: {},
    })
      .then((res) => {
        console.log(res);
        if (res.status == 200) {
          let data = res.data;
          //   console.log(typeof data);
          //   console.log(data.nama);
          setUpdate({
            nama: data.nama,
            email: data.email,
            desc: data.desc,
            // jenisKelamin: data.jenisKelamin,
          });
          setLoading(false);
        }
        Swal.fire("sucess", "Data Berhasil Di Tampilkan", "success");
      })
      .catch((err) => {
        console.log(err);
        nav("/");
        setLoading(false);
      });
  };
  const handleInputData = () => {
    let done = true;
    let dataNew = {
      nama: update.nama,
      email: update.email,
      desc: update.desc,
      // jenisKelamin: update.jenisKelamin,
      // ...update,
    };
    if (done) {
      Swal.fire({
        title: "Apakah anda yakin ingin melakukan update?",
        text: "Data akan tersimpan di database",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, Simpan Data Mahasiswa",
      }).then((result) => {
        if (result.isConfirmed) {
          setLoading(true);
          axios({
            method: "PUT",
            url: `http://localhost:3000/mahasiswa/${id}`,
            headers: {
              "Content-Type": "application/json",
            },
            data: JSON.stringify(dataNew),
          })
            .then((res) => {
              console.log(res);
              //   getDataMahasiswa(res.data.id);
              //   Swal.fire("Succes", "Data Berhasil di Update", "success");
              setLoading(false);
              //   setUpdate({
              //     nama: "",
              //     email: "",
              //     desc: "",
              //   });
              nav("/");
            })
            .catch((err) => {
              console.log(err);
              setLoading(false);
              Swal.fire("Sorry", "Data Gagal Melakukan Update", "error");
            });
        }
      });
    }
  };
  useEffect(() => {
    getMahasiswa();
  }, []);
  return (
    <>
      {/* <h1>Soleh</h1> */}
      {loading ? (
        <h1>Proses Loading</h1>
      ) : (
        <div>
          <form onSubmit={handleSubmit(handleInputData)}>
            <Card className="py-4 my-3">
              <h5>Edit Data Mahasiswa</h5>
              <hr />
              {/* <smal>{id}</smal> */}
              <Row className="mt-3">
                <Col xs="12" lg="4">
                  <Label htmlFor="nama">Nama</Label>
                  <Input
                    id="nama"
                    // name="nama"
                    type="text"
                    // {...register("nama")}
                    value={update.nama}
                    onChange={(t) => {
                      setUpdate({ ...update, nama: t.target.value });
                    }}
                  />
                  {/* <p>{errors.nama?.message}</p> */}
                </Col>
                <Col xs="12" lg="4">
                  <Label htmlFor="email">Alamat Email</Label>
                  <Input
                    id="email"
                    // name="email"
                    type="email"
                    {...register("email")}
                    value={update.email}
                    onChange={(t) => {
                      setUpdate({ ...update, email: t.target.value });
                    }}
                  />
                  <p>{errors.email && <span>Wajib DI sisi</span>}</p>
                </Col>
                <Col xs="12" lg="4">
                  <Label htmlFo r="desc">
                    Keterangan
                  </Label>
                  <Input
                    id="desc"
                    name="desc"
                    type="text"
                    value={update.desc}
                    onChange={(t) => {
                      setUpdate({ ...update, desc: t.target.value });
                    }}
                  />
                </Col>
              </Row>
            </Card>
            <Card className="p-4 my-3">
              <Row className="mt-3">
                <Col xs="12" lg="4">
                  <Label htmlFor="pendidikan">Pendidikan</Label>
                  <Input
                    type="select"
                    name="name"
                    // value={education.pendidikan}
                    className="form-select"
                  >
                    {/* {pendidikan.map((val, index) => {
                    return (
                      <option key={index} value={val.value}>
                        {val.label}
                      </option>
                    );
                  })} */}
                  </Input>
                </Col>
                <Col xs="12" lg="4">
                  {/* <div>
                    <Label>Jenis Kelamain</Label>
                  </div>
                  <div className="d-flex justify-content-evenly w-100 align-items-center">
                    <Input
                      id="gender"
                      name="gender"
                      type="radio"
                      value="l"
                      checked={update?.jenisKelamin === "l"}
                      onClick={() =>
                        setUpdate({ ...update, jenisKelamin: "l" })
                      }
                     
                      className="m-0 p-0"
                    />
                    <Label>Laki - Laki</Label>
                    <Input
                      id="gender"
                      name="gender"
                      type="radio"
                      value="p"
                      checked={update?.jenisKelamin === "p"}
                      onClick={() =>
                        setUpdate({ ...update, jenisKelamin: "p" })
                      }
                      className="m-0 p-0"
                    />
                    <Label>Perempuan</Label>
                  </div> */}
                  {errors?.nama?.type === "required" && (
                    <p>Fild Nama Wajib di isi</p>
                  )}
                  {errors.password && (
                    <Form.Text className="text-danger">
                      {errors.password.message}
                    </Form.Text>
                  )}
                  {errors?.nama?.type === "pattern" && (
                    <p>Wajib huruf alfavet</p>
                  )}
                </Col>
                <Col xs="12" lg="4">
                  <Label htmlFor="jurusan">Jurusan</Label>
                  <Input
                    id="major"
                    name="major"
                    type="select"
                    value={""}
                    {...register("pendidikan")}
                  >
                    <option hidden>{""}</option>
                    <option value="S1">S1</option>
                    <option value="Ekonomi">Ekonomi</option>
                    <option value="Multimedia">Multimedia</option>
                    <option value="IT">TI</option>
                    <option value="Sipil">Sipil</option>
                  </Input>
                </Col>
              </Row>
            </Card>
            <div className="col-xs-12 col-lg-4 d-flex justify-content-between gap-3">
              <Button block color="danger" onClick={() => nav("/")}>
                Cancel
              </Button>

              <Button block color="danger">
                Simpan
              </Button>
            </div>
          </form>
        </div>
      )}
    </>
  );
};
export default EditFormData;
