import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Input, Label, Row, Col, Card, Button, Container } from "reactstrap";
// import Form from "reactstrap";
// import Form from "react-bootstrap/Form";
import Swal from "sweetalert2";
import axios from "axios";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
// import REQUIRED_VALIDATION from "../utils/utils";

const UpdateFrom = () => {
  const initState = {
    nama: "",
    email: "",
    desc: "",
  };
  const nav = useNavigate();
  const [initialValues, setInitialValue] = useState(initState);
  const validationSchema = yup
    .object({
      email: yup.string().email().required("Wajib di isi fild email"),
      nama: yup
        .string()
        .matches(/^[A-Za-a]+$/, "Wajib Alfabet")
        .required("Wajib di isi fild nama")
        .length(5, "minimal 5 karakter")
        .lowercase("harus lowercase ya"),
      password: yup.string().min(6).required(),
      desc: yup
        .string()
        .length(10, "Minimal 5 karaakter yang di input")
        .required("Wajib di isi Fild Deskripsi"),
      confirmPassword: yup
        .string()
        .when("password", (password, field) =>
          password ? field.required().oneOf([yup.ref("passwrd")]) : field
        ),
    })
    .required();

  const handleSubmitData = (values) => {
    // console.log(":::", values);
    console.log("berhasil");
  };
  const handleError = (error) => {
    console.log("ERORR:::");
  };
  const {
    register,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onTouched",
    reValidateMode: "onChange",
    defaultValues: initialValues,
    resolver: yupResolver(validationSchema),
  });

  // const [gender, setGender] = useState();
  const [education, setEducation] = useState({
    nama: "",
    email: "",
    desc: "",
    jenisKelamin: "",
    id: Date.now(),
  });
  const [mahasiswa, setMahasiswa] = useState([]);
  const [loading, setLoading] = useState(false);
  const handleEducation = (event) => {
    let {
      target: { value = "", name = "" },
    } = event;
    setEducation((prevState) => ({ ...prevState, [name]: value }));
  };
  let pendidikan = [
    {
      label: "",
      value: "",
    },
    {
      label: "SD",
      value: "SD",
    },
    {
      label: "SMP",
      value: "SMP",
    },
    {
      label: "SMK",
      value: "SMK",
    },
    {
      label: "D3",
      value: "D3",
    },
    {
      label: "S1",
      value: "S1",
    },
  ];

  const getData = async () => {
    try {
      setLoading(true);
      const response = await axios({
        method: "GET",
        url: `http://localhost:3000/mahasiswa`,
        header: {
          "Content-Type": "application/json",
        },
      });
      if (response.status == 200) {
        let res = response.data;
        console.log(res);
        setMahasiswa(res);
        setLoading(false);
      } else {
        Swal.fire("Sorry", `Status Response ${response.status} di coba Lagi`);
      }
    } catch (err) {
      console.error(err);
      Swal.fire("Error", "Terjadi kesalahan pencarian data", "error");
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const submitMainData = async (e) => {
    // e.preventDefault();
    let data = {
      // nama: education.nama,
      // email: education.email,
      // desc: education.desc,
      // jenisKelamin: watch("jenisKelamin") === "Male" ? "true" : "false",
      // id: Date.now(),

      ...education,
      // jenisKelamin: watch("jenisKelamin") === "Male" ? "true" : "false",
    };
    // watch("jenisKelamin") === "Male" ? "true" : "false";
    setLoading(true);
    await axios({
      url: "http://localhost:3000/mahasiswa",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify(data),
    })
      .then((res) => {
        console.log(res);
        getData(res.data.id);
        setLoading(false);
        //Setelah data di input dan berhasil disimpan, kosongkan lagi datanya
        setEducation({
          nama: "",
          email: "",
          desc: "",
          jenisKelamin: "",
        });
        Swal.fire("Sukses", "Data mahasiswa berhasil disimpan", "success");
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
        Swal.fire("Sorry", "Data Gagal di tempilkan", "error");
      });
  };
  const deleteData = (id) => {
    Swal.fire({
      title: "Apakah Anda Yakin Ingin Delete?",
      text: "Anda tidak bisa membatalkan aksi ini!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      // alert(id);
      if (result.isConfirmed) {
        setLoading(true);
        axios({
          method: "DELETE",
          url: ` http://localhost:3000/mahasiswa/${id}`,
          headers: {
            Content_Type: "application/json",
          },
          data: {},
        })
          // await fetch({
          //   method: "DELETE",
          //   url: ` http://localhost:3000/mahasiswa/`,
          //   headers: {
          //     Content_Type: "application/json",
          //   },
          //   body: JSON.stringify({ id: id }),
          // })
          .then((res) => {
            console.log(res);
            Swal.fire("Berhasil", "Data Berhasil Delete", "success");
            getData();
            setLoading(false);
          })
          .catch((error) => {
            Swal.fire("Sorry", "Data gagal terdelete", "error");
            console.log(error);
            setLoading(false);
          });
      }
    });
  };
  const handleEdit = (id) => {
    // alert(id);
    nav(`/editmahasiswa/${id}`);
  };
  //let CheckStatus = education?.jenisKelamin; //? "true" : "false";
  return (
    <>
      {loading ? (
        <h1>Loading</h1>
      ) : (
        <form
          onSubmit={handleSubmit(handleSubmitData, handleError)}
          style={{
            display: "flex",
            flexDirection: "column",
            margin: "20px 100px",
          }}
        >
          <label>Nama :</label>
          <input
            type="text"
            placeholder="Masukan Nama Anda"
            {...register("nama")}
          />
          {errors?.nama?.type === "required" && <p>Fild Nama Wajib di isi</p>}
          <label>Alamat Email :</label>
          <input
            type="email"
            placeholder="Masukan Email Anda"
            {...register("email")}
          />
          {errors.email && <p>{errors.email.message}</p>}
          <label>Deskripsi</label>
          <textarea rows="4" col="5" {...register("desc")} />
          {errors.desc && <p>{errors.desc.message}</p>}
          <button type="submit">Simpan</button>
        </form>
      )}
    </>
  );
};
export default UpdateFrom;
