import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { grey } from "@mui/material/colors";
import { styled } from "@mui/material/styles";
import Stack from "@mui/material/Stack";
import OurClientSatu from "../assets/images/ourclientsatu.png";
import OurClientDua from "../assets/images/ourclientdua.png";
import { makeStyles } from "@mui/styles";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
// import { fontWeight } from "@mui/system";
import Suzuki from "../assets/images/suzuki.png";
import SSI from "../assets/images/ssi.png";
import Sugity from "../assets/images/sugity.png";
import SUI from "../assets/images/subang.png";

const CustomTypograpy = styled(Typography)(({ theme }) => ({
  fontWeight: "bold",
  color: theme.palette.common.black,
  marginBottom: "60px",
}));
const CustomDescription = styled(Typography)(({ theme }) => ({
  fontWeight: "bold",
  color: theme.palette.common.black,
}));
const useStyles = makeStyles({
  myImg: {
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    objectFit: "cover",
    marginLeft: "-30px",
  },
});
const OurClinet = () => {
  const classes = useStyles();
  return (
    <Box>
      <Box sx={{ color: "#383838", textTransform: "uppercase" }}>
        <Container maxWidth="lg" sx={{ py: 2 }}>
          <CustomTypograpy component="h1" variant="h4" align="center">
            Our Clients
          </CustomTypograpy>
          <Grid
            container
            alignItems="start"
            spacing={6}
            sx={{ display: "flex", justifyContent: "center" }}
          >
            <Grid item xs={12} lg={6} md={6}>
              <Card sx={{ display: "flex", paddingLeft: "15px" }}>
                <CardMedia
                  component="img"
                  sx={{ width: "150px", objectFit: "contain" }}
                  image={Suzuki}
                  alt="Suzuki"
                />
                <Box sx={{ display: "flex", flexDirection: "column" }}>
                  <CardContent sx={{ flex: "1 0 auto" }}>
                    <Typography variant="p" component="h4">
                      PT. SUZUKI INDOMOBIL MOTOR MANUFACTURING
                    </Typography>
                    <Typography variant="p" component="p" align="justify">
                      Kawasan Industri GIIC Block AC no.1, Sukamahi, Kec.
                      Cikarang Pusat, Kabupaten Bekasi, Jawa Barat 17350
                    </Typography>
                  </CardContent>
                </Box>
              </Card>
            </Grid>
            <Grid item xs={12} lg={6} md={6}>
              <Card sx={{ display: "flex", paddingLeft: "15px" }}>
                <CardMedia
                  component="img"
                  sx={{ width: 150, width: "150px", objectFit: "contain" }}
                  image={Sugity}
                  alt="Suzuki"
                />
                <Box sx={{ display: "flex", flexDirection: "column" }}>
                  <CardContent sx={{ flex: "1 0 auto" }}>
                    <Typography variant="p" component="h4">
                      PT. SUZUKI INDOMOBIL MOTOR MANUFACTURING
                    </Typography>
                    <Typography variant="p" component="p" align="justify">
                      Kawasan Industri GIIC Block AC no.1, Sukamahi, Kec.
                      Cikarang Pusat, Kabupaten Bekasi, Jawa Barat 17350
                    </Typography>
                  </CardContent>
                </Box>
              </Card>
            </Grid>
            <Grid item xs={12} lg={6} md={6}>
              <Card sx={{ display: "flex", paddingLeft: "15px" }}>
                <CardMedia
                  component="img"
                  sx={{ width: "150px", objectFit: "contain" }}
                  image={SSI}
                  alt="Suzuki"
                />
                <Box sx={{ display: "flex", flexDirection: "column" }}>
                  <CardContent sx={{ flex: "1 0 auto" }}>
                    <Typography variant="p" component="h4">
                      PT. SUZUKI INDOMOBIL MOTOR MANUFACTURING
                    </Typography>
                    <Typography variant="p" component="p" align="justify">
                      Kawasan Industri GIIC Block AC no.1, Sukamahi, Kec.
                      Cikarang Pusat, Kabupaten Bekasi, Jawa Barat 17350
                    </Typography>
                  </CardContent>
                </Box>
              </Card>
            </Grid>
            <Grid item xs={12} lg={6} md={6}>
              <Card sx={{ display: "flex", paddingLeft: "15px" }}>
                <CardMedia
                  component="img"
                  sx={{ width: 150, width: "150px", objectFit: "contain" }}
                  image={Suzuki}
                  alt="Suzuki"
                />
                <Box sx={{ display: "flex", flexDirection: "column" }}>
                  <CardContent sx={{ flex: "1 0 auto" }}>
                    <Typography variant="p" component="h4">
                      PT. SUZUKI INDOMOBIL MOTOR MANUFACTURING
                    </Typography>
                    <Typography variant="p" component="p" align="justify">
                      Kawasan Industri GIIC Block AC no.1, Sukamahi, Kec.
                      Cikarang Pusat, Kabupaten Bekasi, Jawa Barat 17350
                    </Typography>
                  </CardContent>
                </Box>
              </Card>
            </Grid>
            <Grid item xs={12} lg={6} md={6}>
              <Card sx={{ display: "flex", paddingLeft: "15px" }}>
                <CardMedia
                  component="img"
                  sx={{ width: "150px", objectFit: "contain" }}
                  image={SUI}
                  alt="Suzuki"
                />
                <Box sx={{ display: "flex", flexDirection: "column" }}>
                  <CardContent sx={{ flex: "1 0 auto" }}>
                    <Typography variant="p" component="h4">
                      PT. SUZUKI INDOMOBIL MOTOR MANUFACTURING
                    </Typography>
                    <Typography variant="p" component="p" align="justify">
                      Kawasan Industri GIIC Block AC no.1, Sukamahi, Kec.
                      Cikarang Pusat, Kabupaten Bekasi, Jawa Barat 17350
                    </Typography>
                  </CardContent>
                </Box>
              </Card>
            </Grid>
            <Grid item xs={12} lg={6} md={6}>
              <Card sx={{ display: "flex", paddingLeft: "15px" }}>
                <CardMedia
                  component="img"
                  sx={{ width: 150, width: "150px", objectFit: "contain" }}
                  image={Suzuki}
                  alt="Suzuki"
                />
                <Box sx={{ display: "flex", flexDirection: "column" }}>
                  <CardContent sx={{ flex: "1 0 auto" }}>
                    <Typography variant="p" component="h4">
                      PT. SUZUKI INDOMOBIL MOTOR MANUFACTURING
                    </Typography>
                    <Typography variant="p" component="p" align="justify">
                      Kawasan Industri GIIC Block AC no.1, Sukamahi, Kec.
                      Cikarang Pusat, Kabupaten Bekasi, Jawa Barat 17350
                    </Typography>
                  </CardContent>
                </Box>
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box sx={{ color: "#383838", textTransform: "capitalize" }}>
        <Container maxWidth="lg" sx={{ py: 12 }}>
          <CustomTypograpy component="h1" variant="h4" align="center">
            What Our Customers Stay
          </CustomTypograpy>
          <Grid
            container
            alignItems="center"
            columnSpacing={4}
            sx={{
              backgroundColor: "#ECF0FF",
            }}
          >
            <Grid item xs={6} lg="auto" md="auto">
              <img
                src={OurClientSatu}
                alt="OurClietSatu"
                width="100%"
                height="auto"
                // className={classes.myImg}
              />
            </Grid>
            <Grid item xs={6} lg={3} md={3}>
              <Typography
                variant="body1"
                align="justify"
                sx={{ marginBottom: "30px" }}
              >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id sed
                ac facilisi sit mattis. Aenean turpis nisl non a blandit
                interdum non elementum, rhoncus. Senectus lectus faucibus arcu
                fermentum. Dui sed nulla vitae ut. Et id ridiculus et sed
                tincidunt tortor non vel lacinia.
              </Typography>
              <CustomDescription variant="h6">
                Iman Rahman Alkatiri
              </CustomDescription>
              <Typography variant="p">CEO of Digiweherehouse</Typography>
            </Grid>
            <Grid item xs={6} lg="auto" md="auto">
              <img
                src={OurClientDua}
                alt="OurClietDua"
                width="100%"
                height="auto"
                // className={classes.myImg}
              />
            </Grid>
            <Grid item xs={6} lg={3} md={3}>
              <Typography
                variant="body1"
                align="justify"
                sx={{ marginBottom: "30px" }}
              >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id sed
                ac facilisi sit mattis. Aenean turpis nisl non a blandit
                interdum non elementum, rhoncus. Senectus lectus faucibus arcu
                fermentum. Dui sed nulla vitae ut. Et id ridiculus et sed
                tincidunt tortor non vel lacinia.
              </Typography>
              <CustomDescription variant="h6">Meutia Amanda</CustomDescription>
              <Typography variant="p">
                Iventory Planning Manager of Fedex
              </Typography>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Box>
  );
};
export default OurClinet;
