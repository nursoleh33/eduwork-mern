import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { makeStyles } from "@mui/styles";
import Logo from "../assets/images/logo.png";
import Stack from "@mui/material/Stack";
import Toolbar from "@mui/material/Toolbar";
import { styled } from "@mui/material/styles";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  myHeadline: {
    color: "#0237F4",
    fontWeigh: "bold",
    paddingLeft: 5,
  },
  myBox: {
    backgroundColor: "common.black",
  },
  myText: {
    color: "#FFF",
    textDecoration: "none",
    paddingRight: 12,
    fontWeight: "lighter",
    " &:hover": {
      fontWeight: "bold",
    },
  },
});
const CustomTypograpy = styled(Typography)(({ theme }) => ({
  fontWeight: "bold",
  color: theme.palette.common.black,
}));
const Footer = () => {
  const classes = useStyles();
  return (
    <Box>
      <Box
        sx={{
          color: "#383838",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Container maxWidth="lg" sx={{ py: 3 }}>
          <Grid container spacing={16} alignItems="start">
            <Grid item xs={12} lg={5}>
              <Box sx={{ display: "flex", gap: 2, alignItems: "center" }}>
                <img src={Logo} alt="SUGI" width="50px" height="50px" />
                <Typography variant="h6">PT. SUGI INTI PERKASA</Typography>
              </Box>
              <Typography
                variant="body2"
                align="justify"
                sx={{ marginBottom: "130px" }}
              >
                We are committed to give our customer the best products with
                competitive cost that you have never had before, guaranteed
                competitive prices is one of our priorities for our customer, we
                are committed on providing best price solution for your need.
              </Typography>
              <Typography variant="h6" component="h6">
                &copy; 2022 - 2023 PT SUGI INTI PERKASA
              </Typography>
            </Grid>
            <Grid item xs={12} lg>
              <Stack spacing={1} sx={{ marginBottom: "50px" }}>
                <CustomTypograpy variant="h6">Products</CustomTypograpy>
                <Typography variant="p">Cutting Tools</Typography>
                <Typography variant="p">Automation</Typography>
                <Typography variant="p">General Consumable</Typography>
                <Typography variant="p">CAD/CAM Software</Typography>
                <Typography variant="p">Industrial AVG Robot</Typography>
                <Typography variant="p">IT Support Solution</Typography>
              </Stack>
              <Stack spacing={1}>
                <CustomTypograpy variant="h6">Company</CustomTypograpy>
                <Typography variant="p">About</Typography>
                <Typography variant="p">Contact</Typography>
                <Typography variant="p">Instagram</Typography>
                <Typography variant="p">Facebook</Typography>
                <Typography variant="p">Linkendln</Typography>
                <Typography variant="p">Youtube</Typography>
              </Stack>
            </Grid>
            <Grid item xs={12} lg>
              <Stack spacing={1} sx={{ marginBottom: "80px" }}>
                <CustomTypograpy variant="h6">Service</CustomTypograpy>
                <Typography variant="p">Compressor Service</Typography>
                <Typography variant="p">Compressor Maintenance</Typography>
                <Typography variant="p">Machine Tools Service</Typography>
                <Typography variant="p">Machine Tools Maintenance</Typography>
                <Typography variant="p">Panel Box Service</Typography>
              </Stack>
              <Stack spacing={1}>
                <CustomTypograpy variant="h6">Office</CustomTypograpy>
                <Typography variant="p">PT SUGI INTI PERKASA</Typography>
                <Typography variant="body1">
                  Jl. MH Thamrin Ruko Roxy Blok B No. 60 Cibatu Lippo Cikarang
                  Bekasi 17530 Indonesia
                </Typography>
              </Stack>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box sx={{ backgroundColor: "common.black", color: "common.white" }}>
        <Container maxWidth="lg" sx={{ py: 3 }}>
          <Grid container alignItems="center" justifyContent="center">
            <Grid item xs={12} lg="auto">
              {/* <Link to="/" className={classes.myText}>
                Privacy Policy
              </Link>
              <Link to="/" className={classes.myText}>
                Term and Conditions
              </Link> */}
              <Typography variant="p">
                COPYRIGHT &copy; 2019-2022 PT SUGI INTI PERKASA. ALL RIGHTS
                RESERVED.
              </Typography>
            </Grid>
            {/* <Grid item xs={6} lg={6}>
              <Typography variant="body1"></Typography>
            </Grid> */}
          </Grid>
        </Container>
      </Box>
    </Box>
  );
};
export default Footer;
