import React from "react";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";
import ProductsSatu from "../assets/images/Productsatu.png";
import ProductsDua from "../assets/images/productsdua.png";
import ProductsTiga from "../assets/images/productstiga.png";
import ProductsEmpat from "../assets/images/productsempat.png";

function CardsProducts() {
  const nav = useNavigate();
  const handleClick = () => {
    nav(`/productdetail`);
  };
  return (
    <Box>
      <Container maxWidth="lg">
        <Grid
          container
          alignItems="center"
          sx={{ display: "flex", justifyContent: "center", marginBottom: 5 }}
        >
          <Grid item xs={6} lg={3} md={3}>
            <Card
              sx={{ maxWidth: 250, marginTop: 4, height: "auto" }}
              onClick={handleClick}
            >
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="100%"
                  image={ProductsSatu}
                  alt="Cutting Tools"
                />
                <CardContent>
                  <Typography
                    variant="p"
                    component="h3"
                    align="center"
                    gutterBottom
                  >
                    OSG AD
                  </Typography>
                  <Typography variant="p">
                    Externally clooled solid carbide drill
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
          <Grid item xs={6} lg={3} md={3}>
            <Card
              sx={{ maxWidth: 250, marginTop: 4, height: "auto" }}
              onClick={handleClick}
            >
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="100%"
                  image={ProductsDua}
                  alt="Cutting Tools"
                />
                <CardContent>
                  <Typography
                    variant="p"
                    component="h3"
                    align="center"
                    gutterBottom
                  >
                    OSG AD
                  </Typography>
                  <Typography variant="p">
                    Externally clooled solid carbide drill
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
          <Grid item xs={6} lg={3} md={3}>
            <Card
              sx={{ maxWidth: 250, marginTop: 4, height: "auto" }}
              onClick={handleClick}
            >
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="100%"
                  image={ProductsTiga}
                  alt="Cutting Tools"
                />
                <CardContent>
                  <Typography
                    variant="p"
                    component="h3"
                    align="center"
                    gutterBottom
                  >
                    OSG AD
                  </Typography>
                  <Typography variant="p">
                    Externally clooled solid carbide drill
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
          <Grid item xs={6} lg={3} md={3}>
            <Card
              sx={{ maxWidth: 250, marginTop: 4, height: "auto" }}
              onClick={handleClick}
            >
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="100%"
                  image={ProductsEmpat}
                  alt="Cutting Tools"
                />
                <CardContent>
                  <Typography
                    variant="p"
                    component="h3"
                    align="center"
                    gutterBottom
                  >
                    OSG AD
                  </Typography>
                  <Typography variant="p">
                    Externally clooled solid carbide drill
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}

export default CardsProducts;
