import React from "react";
import CategoryProducts from "../assets/images/banner.png";
import { makeStyles } from "@mui/styles";
import Box from "@mui/material/Box";

const useStyles = makeStyles({
  myBanner: {
    backgroundRepeat: "no-repeat",
    objectFit: "cover",
    width: "100%",
    // marginTop: "100px",
  },
});

const Banner = () => {
  const classes = useStyles();
  return (
    <Box>
      <img
        src={CategoryProducts}
        alt="Kategory Products"
        width="100%"
        className={classes.myBanner}
      />
    </Box>
  );
};
export default Banner;
