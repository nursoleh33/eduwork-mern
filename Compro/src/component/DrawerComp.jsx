import React, { useState } from "react";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import { Link } from "react-router-dom";
import HomeIcon from "@mui/icons-material/Home";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import MiscellaneousServicesIcon from "@mui/icons-material/MiscellaneousServices";
import PermContactCalendarIcon from "@mui/icons-material/PermContactCalendar";
import InfoIcon from "@mui/icons-material/Info";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import { makeStyles } from "@mui/styles";
import { blue } from "@mui/material/colors";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { Stack } from "@mui/system";

const useStyles = makeStyles({
  myText: {
    color: "white",
    textDecoration: "none",
    textTransform: "uppercase",
    paddingRight: 12,
    fontWeight: "bold",
    " &:hover": {
      color: blue[500],
      fontWeight: "bold",
    },
  },
  myBox: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
});

function DrawerComp() {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  return (
    <>
      <Drawer
        PaperProps={{
          sx: {
            backgroundColor: "rgba(49,49,116,1)",
            paddingTop: "60px",
            paddingLeft: "10px",
          },
        }}
        anchor="left"
        open={open}
        onClose={() => setOpen(false)}
      >
        <Box>
          <Grid container alignItems="start">
            <Grid item xs={2}>
              <List>
                <ListItemButton>
                  <ListItemIcon>
                    <HomeIcon
                      color="white"
                      fontSize="medium"
                      sx={{ paddingLeft: 1 }}
                    />
                  </ListItemIcon>
                  <ListItemText>
                    <Link to="/" className={classes.myText}>
                      Beranda
                    </Link>
                  </ListItemText>
                </ListItemButton>
              </List>
              <List>
                <ListItemButton>
                  <ListItemIcon>
                    <LibraryBooksIcon
                      color="white"
                      fontSize="medium"
                      sx={{ paddingLeft: 1 }}
                    />
                  </ListItemIcon>
                  <ListItemText>
                    <Link to="/" className={classes.myText}>
                      E-Catalogue
                    </Link>
                  </ListItemText>
                </ListItemButton>
              </List>
              <List>
                <ListItemButton>
                  <ListItemIcon>
                    <ShoppingCartIcon
                      color="white"
                      fontSize="medium"
                      sx={{ paddingLeft: 1 }}
                    />
                  </ListItemIcon>
                  <ListItemText>
                    <Link to="/product" className={classes.myText}>
                      Product
                    </Link>
                  </ListItemText>
                </ListItemButton>
              </List>
              <List>
                <ListItemButton>
                  <ListItemIcon>
                    <MiscellaneousServicesIcon
                      color="white"
                      fontSize="medium"
                      sx={{ paddingLeft: 1 }}
                    />
                  </ListItemIcon>
                  <ListItemText>
                    <Link to="/service" className={classes.myText}>
                      Service
                    </Link>
                  </ListItemText>
                </ListItemButton>
              </List>
              <List>
                <ListItemButton>
                  <ListItemIcon>
                    <InfoIcon
                      color="white"
                      fontSize="medium"
                      sx={{ paddingLeft: 1 }}
                    />
                  </ListItemIcon>
                  <ListItemText>
                    <Link to="/about" className={classes.myText}>
                      About
                    </Link>
                  </ListItemText>
                </ListItemButton>
              </List>
              <List>
                <ListItemButton>
                  <ListItemIcon>
                    <PermContactCalendarIcon
                      color="white"
                      fontSize="medium"
                      sx={{ paddingLeft: 1 }}
                    />
                  </ListItemIcon>
                  <ListItemText>
                    <Link to="/contact" className={classes.myText}>
                      Contact
                    </Link>
                  </ListItemText>
                </ListItemButton>
              </List>
              {/* <Stack direction="row" rowGap={3}>
                <Link to="/" className={classes.myText}>
                  Beranda
                </Link>
                <Link to="/product" className={classes.myText}>
                  Product
                </Link>
                <Link to="/service" className={classes.myText}>
                  Services
                </Link>
                <Link to="/" className={classes.myText}>
                  E-Catalogue
                </Link>
                <Link to="/about" className={classes.myText}>
                  Tentang Kami
                </Link>
              </Stack> */}
            </Grid>
          </Grid>
        </Box>
      </Drawer>
      <IconButton
        sx={{ marginLeft: "auto", color: "white" }}
        onClick={() => setOpen(!open)}
      >
        <MenuIcon />
      </IconButton>
    </>
  );
}

export default DrawerComp;
