import React from "react";
import Box from "@mui/system/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/system/Container";
import Typography from "@mui/material/Typography";
import { makeStyles } from "@mui/styles";
import Logo from "../assets/images/Img.png";
import Button from "@mui/material/Button";
import Hero from "../assets/images/Img.png";
const useStyles = makeStyles({
  myImg: {
    backgroundRepeat: "no-repeat",
    objectFit: "cover",
    maxWidth: "100%",
    // height: "900px",
  },
});

function Jumbotron() {
  const classes = useStyles();
  return (
    <Box>
      <Box
        sx={{
          height: { sx: "500px", lg: "900px" },
          marginTop: 13,
          position: "relative",
          "&::after": {
            position: "absolute",
            content: '"',
            width: "100%",
            height: "100%",
            top: 0,
            left: 0,
            background: "rgba(0,0,0,0.4)",
            backgroundImage: `linear-gradient(
              to bottom,
              rgba(0,0,0,0.8)0,
              rgba(0,0,0,0) 60%,
              rgba(0,0,0,0.8) 100%,
            )`,
          },
          // display: "flex",
          // justifyContent: "center",
          // alignItems: "center",
        }}
      >
        <img
          src={Logo}
          alt="Hero"
          width="100%"
          // height="400vh"
          // className={classes.myImg}
          // sx={{ height: { xs: "800px", md: "auto" } }}
          // sx={{ position: "absolute", top: "40" }}
        />
        {/* data */}
        <Container
          maxWidth="md"
          sx={{
            position: "absolute",
            top: { xs: 10, md: 105, lg: 105 },
          }}
        >
          <Grid container alignItems="start">
            <Grid item xs="auto" lg="auto" md="auto">
              <Typography
                variant="h2"
                color="#FFF"
                textAlign="left"
                fontWeight="lighter"
                // sx={{ fontWeight: "bold", marginBottom: 1 }}
              >
                We offer a wide range of
              </Typography>
              <Typography
                variant="h2"
                color="#FFF"
                textAlign="left"
                gutterBottom
                fontWeight="700"
                sx={{
                  backgroundColor: "#FF792B",
                  borderRadius: "10px",
                  maxWidth: "550px",
                }}
              >
                Industrial Solutions
              </Typography>
              <Box
                sx={{
                  color: "common.white",
                  paddingLeft: "20px",
                }}
              >
                <Typography variant="h6" textAlign="left">
                  We offer comprehensive solutions through our
                </Typography>
                <Typography variant="h6" textAlign="left">
                  extensive range of machines and equipment to
                </Typography>
                <Typography variant="h6" textAlign="left" sx={{ mb: 2 }}>
                  serve industrial needs
                </Typography>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexWrap: "wrap",
                  gap: "15px",
                  width: { xs: "200px", md: "200px", lg: "500px" },
                  flexDirection: { xs: "column", md: "column", lg: "row" },
                  // flexDirection: { sm: "row", md: "row", lg: "row" },

                  // flexDirection: { sm: "row", md: "column", lg: "column" },
                }}
              >
                <Button
                  variant="contained"
                  color="primary"
                  size="medium"
                  href="/service"
                  // sx={{ mr: 5 }}
                >
                  See our service
                </Button>
                <Button
                  variant="outlined"
                  color="white"
                  size="medium"
                  href="/contact"
                >
                  Contact Us
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Box>
  );
}
export default Jumbotron;
