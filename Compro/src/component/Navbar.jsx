import React from "react";
import Box from "@mui/system/Box";
import Logo from "../assets/images/logo.png";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";
import { Button, useMediaQuery } from "@mui/material";
import { makeStyles } from "@mui/styles";
import Container from "@mui/material/Container";
import { blue, grey } from "@mui/material/colors";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import MenuIcon from "@mui/icons-material/Menu";
import DrawerComp from "./DrawerComp";
import { useTheme } from "@mui/material";
import { useState } from "react";

const useStyles = makeStyles({
  myText: {
    color: grey[900],
    textDecoration: "none",
    textTransform: "uppercase",
    paddingRight: 25,
    fontWeight: "lighter",
    " &:hover": {
      color: blue[500],
      fontWeight: "bold",
    },
  },
  myHeadline: {
    color: grey[900],
    fontWeight: "bold",
    paddingLeft: 8,
  },
});
const Navbar = () => {
  const classes = useStyles();
  const theme = useTheme();
  let isMatch = useMediaQuery(theme.breakpoints.down("md"));
  console.log(isMatch);
  const [value, setValue] = useState();
  return (
    <Box>
      <AppBar
        sx={{
          padding: "20px 25px",
          position: "fixed",
          width: "100%",
          backgroundColor: "common.white",
        }}
      >
        <Toolbar>
          {isMatch ? (
            <>
              {" "}
              <Grid container alignItems="center" columnGap={2}>
                <Grid item xs={1}>
                  <img
                    src={Logo}
                    alt="PT SUGI INTI PERKASA"
                    width="50px"
                    height="50px"
                  />
                </Grid>
                <Grid item xs>
                  <Typography
                    component="h1"
                    variant="h6"
                    className={classes.myHeadline}
                  >
                    PT. SUGI INTI PERKASA
                  </Typography>
                </Grid>
              </Grid>
              <DrawerComp />
            </>
          ) : (
            <Grid container alignItems="center" spacing={1}>
              <Grid item md="auto" lg="auto">
                <img src={Logo} alt="SUGI" width="50px" height="50px" />
              </Grid>
              <Grid item md="auto" lg="auto">
                <Typography
                  component="h1"
                  variant="h6"
                  className={classes.myHeadline}
                >
                  PT. SUGI INTI PERKASA
                </Typography>
              </Grid>
              <Grid item md lg />
              <Grid
                item
                md="auto"
                lg="auto"
                sx={{ width: "500px", bg: "yellow" }}
              >
                <Link to="/" className={classes.myText}>
                  Home
                </Link>
                <Link to="/product" className={classes.myText}>
                  Product
                </Link>
                <Link to="/service" className={classes.myText}>
                  Services
                </Link>
                <Link to="/" className={classes.myText}>
                  E-Catalogue
                </Link>
                <Link to="/about" className={classes.myText}>
                  About Us
                </Link>
              </Grid>
              <Grid item md="auto" lg="auto">
                <Button
                  variant="contained"
                  color="primary"
                  href="/contact"
                  sx={{ borderRadius: "5px" }}
                >
                  Contact Us
                </Button>
                {/* <Box display="flex">
                  
                </Box> */}
              </Grid>
              {/* <Grid item lg="auto">
              <Button
                variant="contained"
                color="primary"
                sx={{ borderRadius: "5px" }}
              >
                <FileDownloadIcon
                  color="#FFF"
                  fontSize="medium"
                  sx={{ paddingRight: 1 }}
                />
                CATALOGUE
              </Button>
            </Grid> */}
            </Grid>
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
};
export default Navbar;
