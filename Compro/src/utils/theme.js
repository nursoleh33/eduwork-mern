import { yellow, red } from "@mui/material/colors";
import { createTheme, responsiveFontSizes } from "@mui/material/styles";
const base = createTheme({
  palette: {
    drank: {
      main: "#202021",
    },
    secondary: {
      main: "#25D366",
    },
    primary: {
      main: "#0038FD",
    },
    white: {
      main: "#FFF",
    },
    tertty: {
      main: "#FFFFFFF",
    },
    blue: {
      main: "#ECF0FF",
    },
    bluedongker: {
      main: "#002F52",
    },
    //misalnau untuk status error atau sukses
    error: {
      main: red[900],
    },
    breakpoints: {
      value: {
        xs: 0,
        sm: 720,
        md: 960,
        lg: 1100,
        xl: 1500,
      },
    },
  },
});
// setup untuk explore agar bi sa di gunakan di component lain
const theme = responsiveFontSizes(base);
export default theme;
