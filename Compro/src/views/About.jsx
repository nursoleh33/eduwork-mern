import React from "react";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import About from "../assets/images/about.png";
import Satu from "../assets/images/satu.png";
import Dua from "../assets/images/dua.png";
import Tiga from "../assets/images/tiga.png";
import { styled } from "@mui/material/styles";
import SugiIntiPerkasa from "../assets/images/sugiintiperkasa.png";
import Navbar from "../component/Navbar";
import Cards from "../component/Cards";
import { makeStyles } from "@mui/styles";
import { List, ListItem, ListItemAvatar, ListItemText } from "@mui/material";
import Button from "@mui/material/Button";
import Footer from "../parts/Footer";

const CustomTypograpy = styled(Typography)(() => ({
  textAlign: { xs: "center", md: "lef" },
}));
const Title = ({ text }) => (
  <CustomTypograpy
    variant="h4"
    component="h1"
    gutterBottom
    sx={{ textAlign: { xs: "center", md: "left" } }}
  >
    {text}
  </CustomTypograpy>
);
const Description = ({ text, desc }) => (
  <ListItemText
    primary={text}
    secondary={desc}
    sx={{ textAlign: "justify" }}
  ></ListItemText>
);
const useStyles = makeStyles({
  myImg: {
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    objectFit: "contain",
  },
  myProduct: {
    paddingRight: 15,
  },
});

function AboutUs() {
  const classes = useStyles();
  return (
    <Box>
      <Navbar />
      <Cards
        title="ABOUT US"
        desc="Sugi Inti Pekasa is the best product and services you have never had before! It is committed to giving you the best products with competitive costs that you have never had before. It is guaranteed that their competitive prices are one of their priorities for their customers. They are committed on providing the best price solution for your need."
      />
      <Box>
        <Container maxWidth="lg">
          <Grid
            container
            alignItems="center"
            columnSpacing={12}
            sx={{ marginTop: "-50px" }}
          >
            <Grid item xs={12} lg={6} md={6} sx={{ order: { xs: 2, md: 1 } }}>
              <img
                src={SugiIntiPerkasa}
                alt="Products"
                width="100%"
                height="800px"
                className={classes.myImg}
                sx={{ textAlign: { sx: "center", md: "left" } }}
              />
            </Grid>
            <Grid item xs={12} lg={6} md={6} sx={{ order: { xs: 1, md: 2 } }}>
              <Title
                text="We are committed to giving you the best products that you have
                    ever had befoure."
              />
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    {/* <Avatar
                            alt="It support"
                            src={Support}
                            sx={{ width: "50px" }}
                        /> */}
                    <img
                      src={Satu}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Quality"
                    desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parturient non, nulla lacus tellus donec. Egestas tempor pellentesque cras adipiscing."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    {/* <Avatar
                            alt="It support"
                            src={Support}
                            sx={{ width: "50px" }}
                        /> */}
                    <img
                      src={Dua}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Competitive"
                    desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parturient non, nulla lacus tellus donec. Egestas tempor pellentesque cras adipiscing."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    {/* <Avatar
                            alt="It support"
                            src={Support}
                            sx={{ width: "50px" }}
                        /> */}
                    <img
                      src={Tiga}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Competitive"
                    desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parturient non, nulla lacus tellus donec. Egestas tempor pellentesque cras adipiscing."
                  />
                </ListItem>
              </List>
              <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
                <Button
                  variant="contained"
                  color="primary"
                  size="large"
                  // startIcon={AccessAlarmIcon}
                  sx={{
                    fontWeight: "bold",
                  }}
                >
                  More Info
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box sx={{ color: "#383838" }}>
        <Container maxWidth="lg" sx={{ pb: 7 }}>
          <Grid container alignItems="start" columnSpacing={25}>
            <Grid item xs={12} lg={6} md={6}>
              <Box>
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.486835806959!2d107.14702201408083!3d-6.330912763706514!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e699ba840f98c8d%3A0x9b9f32f17519cff8!2sPT.%20Sugi%20Inti%20Perkasa!5e0!3m2!1sid!2sid!4v1663349600212!5m2!1sid!2sid"
                  width="600"
                  height="350"
                ></iframe>
              </Box>
            </Grid>
            <Grid item xs={12} lg={6} md={6}>
              <Typography variant="h4" gutterBottom>
                Find us on maps
              </Typography>
              <Typography variant="body1" component="h1" gutterBottom>
                PT. SUGI INTI PERKASA
              </Typography>
              <Typography
                variant="body1"
                sx={{ textAlign: "justify", textTransform: "capitalize" }}
              >
                Jl. MH Thamrin Ruko Roxy Blok B No. 60 Cibatu Lippo Cikarang
                Bekasi 17530 Indonesia
              </Typography>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Footer />
    </Box>
  );
}

export default AboutUs;
