import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import Button from "@mui/material/Button";
import { makeStyles, styled } from "@mui/styles";
import { Link } from "react-router-dom";
import { blue, grey } from "@mui/material/colors";
import Stack from "@mui/material/Stack";
import CuttingToolsProduct from "../assets/images/cuttingtoolsproducts.png";
import Navbar from "../component/Navbar";
import OurClinet from "../parts/OurClinet";
import Footer from "../parts/Footer";
import CardsProducts from "../component/CardsProducts";

const CustomTypograpy = styled(Typography)(({ theme }) => ({
  fontWeight: "bold",
  color: theme.palette.common.black,
  textTransform: "uppercase",
}));
const useStyles = makeStyles({
  myLink: {
    color: blue[900],
    fontWeight: "bold",
    textDecoration: "none",
  },
  myText: {
    textDecoration: "none",
    color: grey[900],
  },
  myBanner: {
    backgroundRepeat: "no-repeat",
    objectFit: "cover",
    width: "100%",
  },
  myBox: {
    padding: "20px 0",
    color: "#FFF",
    width: "100%",
    background: `linear-gradient(90deg, #0237F4 -0.87%, rgba(2, 55, 244, 0) 100%)`,
    margin: "30px 0",
  },
});

function CuttingToolsProducts() {
  const classes = useStyles();
  return (
    <Box sx={{ color: "#383838" }}>
      <Navbar />
      <Container
        maxWidth="lg"
        sx={{ paddingTop: "90px", textTransform: "uppercase" }}
      >
        <CustomTypograpy variant="h4" component="h1" align="center">
          Cutting Tools Poducts
        </CustomTypograpy>
      </Container>
      <Box>
        <img
          src={CuttingToolsProduct}
          alt="CuttinToolsProuct"
          width="100%"
          className={classes.myBanner}
        />
      </Box>
      <Box>
        <Container maxWidth="lg">
          <Stack spacing={2}>
            <Breadcrumbs
              aria-label="Tools Proucts"
              separator={<ArrowForwardIosIcon fontSize="small" />}
            >
              <Link underline="hover" to="/home" className={classes.myText}>
                Home
              </Link>
              <Link underline="hover" to="/product" className={classes.myText}>
                Products
              </Link>
              <Link
                underline="hover"
                to="/cuttingtools"
                className={classes.myLink}
              >
                Cutting Tools
              </Link>
            </Breadcrumbs>
          </Stack>
        </Container>
        <Box className={classes.myBox}>
          <Container maxWidth="lg">
            <Typography variant="h5" align="left" sx={{ fontStyle: "oblique" }}>
              Solid | Indexable | Tapping & Reaming | Special Knife Tools
            </Typography>
          </Container>
        </Box>
        <CardsProducts />
        <CardsProducts />
        <CardsProducts />
        <CardsProducts />
      </Box>
      <OurClinet />
      <Footer />
    </Box>
  );
}

export default CuttingToolsProducts;
