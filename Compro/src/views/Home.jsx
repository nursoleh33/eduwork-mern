import React from "react";
import Navbar from "../component/Navbar";
import Box from "@mui/material/Box";
import Hero from "../assets/images/Img.png";
import Container from "@mui/material/Container";
import Footer from "../parts/Footer";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import { makeStyles } from "@mui/styles";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import ImageIcon from "@mui/icons-material/Image";
import Products from "../assets/images/product.png";
// import Banner from "../assets/images/banner.png";
import Banner from "../component/Banner";
import Support from "../assets/images/itsupport.png";
import CuttingTools from "../assets/images/cuttingtools.png";
import Automation from "../assets/images/automation.png";
import GeneralConsume from "../assets/images/general_consumable.png";
import AccessAlarmIcon from "@mui/icons-material/AccessAlarm";
import Cad from "../assets/images/cadcam.png";
import Industrial from "../assets/images/idustrial.png";
import { red } from "@mui/material/colors";
import { borderRadius } from "@mui/system";
import Button from "@mui/material/Button";
import SugiInti from "../assets/images/sugiinti.png";
import Service from "../assets/images/Service.png";
import Jumbotron from "../component/Hero";
import SugiIntiPerkasa from "../assets/images/sugiintiperkasa.png";
import Satu from "../assets/images/satu.png";
import Dua from "../assets/images/dua.png";
import Tiga from "../assets/images/tiga.png";
import { styled } from "@mui/material/styles";
import OurClinet from "../parts/OurClinet";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";

const useStyles = makeStyles({
  myImg: {
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    objectFit: "contain",
    marginBottom: "30px",
  },
  myBanner: {
    backgroundRepeat: "no-repeat",
    objectFit: "cover",
    width: "100%",
  },
  myGrid: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  myProduct: {
    paddingRight: 15,
  },
  myContent: {
    backgroundColor: "#ECF0FF",
    color: "common.black",
  },
});
//Membuat Style Reusabled digunakan untuk membuat 1 style component yang dapat digunakan di semua component dari pada buat class setiap masing2 component
const CustomTypograpy = styled(Typography)(() => ({
  textAlign: { xs: "center", md: "left" },
}));

const Title = ({ text }) => (
  <CustomTypograpy
    variant="h4"
    component="h1"
    gutterBottom
    sx={{
      textAlign: { xs: "left", md: "left" },
      width: { xs: "500px", lg: "100%", md: "500px" },
      marginLeft: "23px",
    }}
  >
    {text}
  </CustomTypograpy>
);
const Description = ({ text, desc }) => (
  <ListItemText
    primary={text}
    secondary={desc}
    sx={{ textAlign: "justify" }}
  ></ListItemText>
);
function Home() {
  const classes = useStyles();
  return (
    <Box>
      <Navbar />
      <Jumbotron />
      <Box
        sx={{
          backgroundColor: "background.paper",
          color: "common.black",
        }}
      >
        <Container maxWidth="xl" sx={{ pb: 6 }}>
          <Grid container alignItems="center">
            {/* <Grid item xs={12} lg={6} md={6}></Grid> */}
            <Grid
              item
              xs={12}
              lg={8}
              md={6}
              sx={{ marginLeft: { lg: -23 }, order: { xs: 1, md: 1 } }}
            >
              <img
                src={Products}
                alt="Products"
                width="100%"
                height="800px"
                className={classes.myImg}
              />
            </Grid>
            <Grid item xs={12} lg={5} md={9} sx={{ order: { xs: 2, md: 2 } }}>
              <Title text="We provide various of Industrial Maintenance and Services Products." />
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={CuttingTools}
                      alt="Cutting Tools"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Cutting Tools"
                    desc="comprehensive cutting tool manufacuturer specialized in the manufacture and sale of taps, drills, endmills, indexable tools and rolling dies."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    {/* <Avatar
                          alt="It support"
                          src={Support}
                          sx={{ width: "50px" }}
                        /> */}
                    <img
                      src={Automation}
                      alt="Automation"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Automation"
                    desc="We provide system automation services in terms of software and hardware according to your business needs. Our Desktop, mobile, web interface is ready."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={GeneralConsume}
                      alt="GeneralConsume"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="General Consumable"
                    desc="We provide various of Industrial General Consumable and provide Quality | Cost | Delivery | Services manner."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Cad}
                      alt="CAD/CAM Software Solution"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="CAD/CAM Software Solution"
                    desc="Our CAD/CAM software products and automatic nesting applications are improving the productivity of cutting systems, sheet metal machines and welding robots."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Industrial}
                      alt="Industrial AVG Robot Solution"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Cutting Tools"
                    desc="Robotics is a diverse industry, and its future is full of uncertainty: no one can predict where and where it will go in the years ahead."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Support}
                      alt="It Support"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="IT-Support & Solution"
                    desc="We provide various of Industrial ERP System customized base on customer needs."
                  />
                </ListItem>
              </List>
              <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
                <Button
                  variant="contained"
                  color="primary"
                  href="/product"
                  size="large"
                  // startIcon={AccessAlarmIcon}
                  sx={{
                    fontWeight: "bold",
                  }}
                >
                  More Info
                  <ArrowCircleRightIcon
                    color="#FFF"
                    fontSize="medium"
                    sx={{ paddingLeft: 2 }}
                  />
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Banner />
      {/* <Box>
          <img
            src={Banner}
            alt="Products"
            width="100%"
            // height="auto"
            className={classes.myBanner}
          />
        </Box> */}
      <Box className={classes.myContent} sx={{ pb: 6 }}>
        <Container maxWidth="lg">
          <Grid container alignItems="center" columnSpacing={12}>
            <Grid item xs={12} lg={6} md={6} sx={{ order: { xs: 2, md: 2 } }}>
              <Title text="We provide various of Industrial Maintenance and services." />
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Service}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Compressor Maintenance & Service"
                    desc="Terima jasa service compressor / service komprsor udara dengan layanan terbaik dan suku cadang berkualitas. Maintenance, repair overhaul dsb."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Service}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Machine Tool Maintenance & Service"
                    desc="Terima jasa service compressor / servis kompresor udara dengan layanan terbaik dan suku cadang berkualitas. Maintenance, repair, overhaul  dsb."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Service}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Panel Box Maintenace & Service"
                    desc="Terima jasa service compressor / service kompresor udara dengan layanan terbaik dan suku cadang berkulitas. Maintenace, repair, overhaul dsb."
                  />
                </ListItem>
              </List>
              <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
                <Button
                  variant="contained"
                  color="primary"
                  size="large"
                  href="/service"
                  // startIcon={AccessAlarmIcon}
                  sx={{
                    fontWeight: "bold",
                  }}
                >
                  More Info
                  <ArrowCircleRightIcon
                    color="#FFF"
                    fontSize="medium"
                    sx={{ paddingLeft: 2 }}
                  />
                </Button>
              </Box>
            </Grid>
            <Grid item xs={12} lg={6} md={6} sx={{ order: { xs: 1, md: 1 } }}>
              <img
                src={SugiInti}
                alt="Products"
                width="100%"
                height="800px"
                className={classes.myImg}
              />
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box>
        <Container maxWidth="lg">
          <Grid container alignItems="center" columnSpacing={12}>
            <Grid item xs={12} lg={6} md={6} sx={{ order: { xs: 2, md: 1 } }}>
              <img
                src={SugiIntiPerkasa}
                alt="Products"
                width="100%"
                height="800px"
                className={classes.myImg}
                sx={{ textAlign: { sx: "center", md: "left" } }}
              />
            </Grid>
            <Grid item xs={12} lg={6} md={6} sx={{ order: { xs: 1, md: 2 } }}>
              <Title
                text="We are committed to giving you the best products that you have
                  ever had befoure."
              />
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    {/* <Avatar
                          alt="It support"
                          src={Support}
                          sx={{ width: "50px" }}
                        /> */}
                    <img
                      src={Satu}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Quality"
                    desc="Terima jasa service compressor / service komprsor udara dengan layanan terbaik dan suku cadang berkualitas. Maintenance, repair overhaul dsb."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Dua}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Competitive"
                    desc="Terima jasa service compressor / servis kompresor udara dengan layanan terbaik dan suku cadang berkualitas. Maintenance, repair, overhaul  dsb."
                  />
                </ListItem>
              </List>
              <List
                sx={{
                  width: "100%",
                  maxWidth: 450,
                  // bgcolor: "background.paper",
                }}
              >
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <img
                      src={Tiga}
                      alt="Service"
                      width="auto"
                      className={classes.myProduct}
                    />
                  </ListItemAvatar>
                  <Description
                    text="Technical Support"
                    desc="Terima jasa service compressor / service kompresor udara dengan layanan terbaik dan suku cadang berkulitas. Maintenace, repair, overhaul dsb."
                  />
                </ListItem>
              </List>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <OurClinet />
      <Footer />
    </Box>
  );
}

export default Home;
