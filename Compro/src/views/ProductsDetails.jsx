import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import BannerDetails from "../assets/images/bannerDetails.png";
import { makeStyles, styled } from "@mui/styles";
import { red, grey } from "@mui/material/colors";
import Navbar from "../component/Navbar";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Tab from "@material-ui/core/Tab";
import TabContext from "@material-ui/lab/TabContext";
import ListItemText from "@mui/material/ListItemText";
import TabList from "@material-ui/lab/TabList";
import TabPanel from "@material-ui/lab/TabPanel";
import Tabs from "@mui/material/Tabs";
import CardActionArea from "@mui/material/CardActionArea";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ProductSatu from "../assets/images/Productsatu.png";
import FiberManualRecordSharpIcon from "@mui/icons-material/FiberManualRecordSharp";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Stack from "@mui/material/Stack";
import Chip from "@mui/material/Chip";
import ListItemIcon from "@mui/material/ListItemIcon";
import theme from "../utils/theme";
import CardsProducts from "../component/CardsProducts";
import Footer from "../parts/Footer";

const useStyles = makeStyles({
  myBanner: {
    marginTop: "105px",
    backgroundAttachment: "fixed",
    backgroundColor: "#129BFF",
    height: "200px",
    width: "100%",
    marginBottom: 4,
  },
  myImage: {
    objectFit: "cover",
    height: "auto",
    width: "100%",
  },
});

const CustomTypograpy = styled(Typography)(({ theme }) => ({
  fontWeight: "bold",
  color: theme.palette.primary.main,
  textTransform: "uppercase",
}));
const ProductsDetails = () => {
  const [value, setValue] = useState("1");
  const HandleChange = (event, newValue) => {
    setValue(newValue);
  };
  const classes = useStyles();
  return (
    <Box>
      <Navbar />
      <Box className={classes.myBanner}>
        <img
          src={BannerDetails}
          alt="Banner"
          width="100%"
          height="auto"
          className={classes.myImage}
        />
      </Box>
      <Box>
        <Container maxWidth="lg" sx={{ py: 2 }}>
          <Grid container alignItems="start">
            <Grid item xs={12} lg={4} md={4}>
              <Card sx={{ maxWidth: 250, height: "auto" }} raised={true}>
                <CardMedia
                  component="img"
                  height="100%"
                  image={ProductSatu}
                  alt="product"
                />
              </Card>
            </Grid>
            <Grid item xs={12} md={8} lg={8}>
              <Typography
                variant="h6"
                align="left"
                gutterBottom
                sx={{ fontWeight: "bold" }}
              >
                OSG
              </Typography>
              <Typography
                variant="h6"
                gutterBottom
                color="#0B247B"
                sx={{ textTransform: "uppercase" }}
              >
                TAP EX-LT-SFT HSE M8X1.25X100 13880
              </Typography>
              <Stack direction="row" rowGap={3}>
                <Chip label="Available Stock" color="secondary" />
              </Stack>
              <TabContext value={value}>
                <Box
                  sx={{
                    borderBottom: 1,
                    borderColor: "divider",
                    color: "info.main",
                  }}
                >
                  <TabList onChange={HandleChange} aria-label="Product Tab">
                    <Tab label="Overview" value="1" />
                    <Tab label="Aplications" value="2" />
                  </TabList>
                </Box>
                <TabPanel value="1">
                  <Container maxWidth="md" sx={{ ml: "-53px" }}>
                    <Stack direction="row">
                      <Chip
                        label="Overview"
                        size="medium"
                        variant="filled"
                        sx={{
                          width: "100%",
                          borderRadius: "3px",
                          backgroundColor: "#ECECEC",
                        }}
                      />
                    </Stack>
                    <Box sx={{ maxWidth: "400px" }}>
                      <Typography component="p">
                        EXOTAP® VC-10 Ti is the premium solution for Titanium
                        alloys and Heat Resistant Super Alloys.
                      </Typography>
                      <Typography
                        variant="h6"
                        sx={{
                          color: "primary.main",
                          marginTop: 2,
                          fontWeight: "bold",
                        }}
                      >
                        Feature
                      </Typography>
                      <Stack>
                        <List>
                          <ListItem sx={{ marginTop: "-20px" }}>
                            <ListItemIcon>
                              <FiberManualRecordSharpIcon
                                color="primary"
                                fontSize="small"
                              />
                            </ListItemIcon>
                            <ListItemText
                              primary="Premium Powdered Metal VC-10 substrate"
                              sx={{
                                marginLeft: "-30px",
                                color: "primary.main",
                              }}
                            />
                          </ListItem>
                          <ListItem sx={{ marginTop: "-20px" }}>
                            <ListItemIcon>
                              <FiberManualRecordSharpIcon
                                color="primary"
                                fontSize="small"
                              />
                            </ListItemIcon>
                            <ListItemText
                              primary="Spiral Flute"
                              sx={{
                                marginLeft: "-30px",
                                color: "primary.main",
                              }}
                            />
                          </ListItem>
                          <ListItem sx={{ marginTop: "-20px" }}>
                            <ListItemIcon>
                              <FiberManualRecordSharpIcon
                                color="primary"
                                fontSize="small"
                              />
                            </ListItemIcon>
                            <ListItemText
                              primary="OSG V-coating"
                              sx={{
                                marginLeft: "-30px",
                                color: "primary.main",
                              }}
                            />
                          </ListItem>
                          <ListItem sx={{ marginTop: "-20px" }}>
                            <ListItemIcon>
                              <FiberManualRecordSharpIcon
                                color="primary"
                                fontSize="small"
                              />
                            </ListItemIcon>
                            <ListItemText
                              primary="DIN Overall Length"
                              sx={{
                                marginLeft: "-30px",
                                color: "primary.main",
                              }}
                            />
                          </ListItem>
                          <ListItem sx={{ marginTop: "-20px" }}>
                            <ListItemIcon>
                              <FiberManualRecordSharpIcon
                                color="primary"
                                fontSize="small"
                              />
                            </ListItemIcon>
                            <ListItemText
                              primary="Inch & Metric"
                              sx={{
                                marginLeft: "-30px",
                                color: "primary.main",
                              }}
                            />
                          </ListItem>
                        </List>
                      </Stack>
                    </Box>
                  </Container>
                </TabPanel>
                <TabPanel value="2">
                  <Container maxWidth="md" sx={{ ml: "-53px" }}>
                    <Stack direction="row">
                      <Chip
                        label="Applications"
                        size="medium"
                        variant="filled"
                        sx={{
                          width: "100%",
                          borderRadius: "3px",
                          backgroundColor: "#ECECEC",
                        }}
                      />
                    </Stack>
                    <Stack>
                      <List>
                        <ListItem sx={{ marginTop: "-20px" }}>
                          <ListItemIcon>
                            <FiberManualRecordSharpIcon />
                          </ListItemIcon>
                          <ListItemText
                            primary="Titanium, High Nickel Alloy"
                            sx={{ marginLeft: "-30px" }}
                          />
                        </ListItem>
                        <ListItem sx={{ marginTop: "-20px" }}>
                          <ListItemIcon>
                            <FiberManualRecordSharpIcon />
                          </ListItemIcon>
                          <ListItemText
                            primary="Alloy Steels, SS 17-4PH, Hardened Steels up to 45HrC"
                            sx={{ marginLeft: "-30px" }}
                          />
                        </ListItem>
                        <ListItem sx={{ marginTop: "-20px" }}>
                          <ListItemIcon>
                            <FiberManualRecordSharpIcon />
                          </ListItemIcon>
                          <ListItemText
                            primary="Aerospace, Medical"
                            sx={{ marginLeft: "-30px" }}
                          />
                        </ListItem>
                      </List>
                    </Stack>
                  </Container>
                </TabPanel>
              </TabContext>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box>
        <Container maxWidth="lg" xs={{ py: 4 }}>
          <CustomTypograpy variant="h5" component="h1" align="left">
            SIMILAR PRODUCTS
          </CustomTypograpy>
          <CardsProducts />
        </Container>
        <Container maxWidth="lg" xs={{ py: 2 }}>
          <CustomTypograpy variant="h5" component="h1" align="left">
            RECOMMENDED PRODUCTS
          </CustomTypograpy>
          <CardsProducts />
        </Container>
      </Box>
      <Footer />
    </Box>
  );
};

export default ProductsDetails;
