import React from "react";
import { useNavigate } from "react-router-dom";
import Banner from "../component/Banner";
import Box from "@mui/material/Box";
import Navbar from "../component/Navbar";
import { makeStyles, styled } from "@mui/styles";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Link from "@mui/material/Link";
import { blue, grey, red } from "@mui/material/colors";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";
import Automation from "../assets/images/AC.png";
import CuttingTools from "../assets/images/CP.png";
import Painting from "../assets/images/PE.png";
import GeneralConsumbles from "../assets/images/GC.png";
import OurClinet from "../parts/OurClinet";
import Footer from "../parts/Footer";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";

const CustomTypograpy = styled(Typography)(({ theme }) => ({
  fontWeight: "bold",
  color: theme.palette.common.black,
  textTransform: "uppercase",
}));
const useStyles = makeStyles({
  myLink: {
    color: red[900],
    fontWeight: "bold",
    textDecoration: "none",
  },
  myData: {
    textDecoration: "none",
    color: grey[900],
  },
});
const Products = () => {
  const classes = useStyles();
  const nav = useNavigate();
  const handleProducts = () => {
    nav(`/cuttingtools`);
  };
  return (
    <Box>
      <Navbar />
      <Container maxWidth="lg" sx={{ paddingTop: "140px" }}>
        <CustomTypograpy variant="h4" component="h1" align="center">
          Products Solutions
        </CustomTypograpy>
      </Container>
      <Banner />
      <Box>
        <Container maxWidth="lg">
          <Breadcrumbs aria-label="Product">
            <Link underline="hover" to="/" className={classes.myData}>
              Home
            </Link>
            <Link underline="hover" to="/product" className={classes.myLink}>
              Product
            </Link>
          </Breadcrumbs>
          <Grid
            container
            alignItems="center"
            spacing={6}
            sx={{ display: "flex", justifyContent: "center", marginBottom: 5 }}
          >
            <Grid item xs={6} lg={3} md={3}>
              <Card
                sx={{
                  maxWidth: 240,
                  marginTop: 4,
                  height: 350,
                }}
                raised={true}
                onClick={handleProducts}
              >
                <CardActionArea>
                  <CardMedia
                    component="img"
                    height="100%"
                    image={CuttingTools}
                    alt="Cutting Tools"
                  />
                  <CardContent>
                    <Typography variant="h5" align="center">
                      Cutting Tools
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item xs={6} lg={3} md={3}>
              <Card
                sx={{ maxWidth: 240, marginTop: 4, height: 350 }}
                raised={true}
                onClick={handleProducts}
              >
                <CardActionArea>
                  <CardMedia
                    component="img"
                    height="100%"
                    image={Automation}
                    alt="Cutting Tools"
                  />
                  <CardContent>
                    <Typography variant="h5" align="center">
                      Automation
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item xs={6} lg={3} md={3}>
              <Card
                sx={{ maxWidth: 240, marginTop: 4, height: 350 }}
                raised={true}
                onClick={handleProducts}
              >
                <CardActionArea>
                  <CardMedia
                    component="img"
                    height="100%"
                    image={GeneralConsumbles}
                    alt="Cutting Tools"
                  />
                  <CardContent>
                    <Typography variant="h5" align="center">
                      General Consumbles
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item xs={6} lg={3} md={3}>
              <Card
                sx={{ maxWidth: 240, marginTop: 4, height: 350 }}
                raised={true}
                onClick={handleProducts}
              >
                <CardActionArea>
                  <CardMedia
                    component="img"
                    height="100%"
                    image={Painting}
                    alt="Cutting Tools"
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" align="center">
                      Painting Equipment
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <OurClinet />
      <Footer />
    </Box>
  );
};
export default Products;
