import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import Contacts from "../assets/images/about.png";
import Cards from "../component/Cards";
import { makeStyles } from "@mui/styles";
import { red } from "@mui/material/colors";
import Footer from "../parts/Footer";
import Navbar from "../component/Navbar";
import FormControl from "@mui/material/FormControl";

const useStyles = makeStyles({
  myInput: {
    color: red,
    " &.MuiInput-colorSecondary": {
      color: "secondary",
    },
  },
  myButton: {
    " &::affter, &::before": {
      backgroundColor: "#000",
      content: "",
      display: "inline-block",
      height: "1px",
      position: "relative",
      verticalAlign: "middle",
      width: "50%",
    },
    " &::before": {
      right: "0.5em",
      marginLeft: "-50%",
    },
  },
  myHeader: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    gap: "3px",
    margin: 33,
  },
  myLineLeft: {
    width: "100%",
    height: "1px",
    backgroundColor: "#AFAFAF",
    // display: 'flex',
    // alignItems: 'center',
    // flexDirection: 'row',
    // gap: '3px'
  },
  myLineRight: {
    width: "100%",
    height: "1px",
    backgroundColor: "#AFAFAF",
    // display: 'flex',
    // alignItems: 'center',
    // flexDirection: 'row',
    // gap: '3px'
  },
});

function Contact() {
  const classes = useStyles();
  return (
    <Box>
      <Navbar />
      <Cards
        title="CONTACT US"
        desc="Sugi Inti Pekasa is the best product and services you have never had before! It is committed to giving you the best products with competitive costs that you have never had before. It is guaranteed that their competitive prices are one of their priorities for their customers. They are committed on providing the best price solution for your need."
      />
      <Box>
        <Container maxWidth="lg">
          <Grid container alignItems="start" spacing={3}>
            <Grid item xs={8} md={12} lg={12}>
              <FormControl fullWidth>
                <Box sx={{ mx: 4, my: 2 }}>
                  <TextField
                    fullWidth
                    variant="filled"
                    label="Subject"
                    type="text"
                    placeholder="Masukan Subject Email Anda"
                    autoFocus={true}
                    // error={true}
                    color="primary"
                    // helperText="Please Input Your Subject Email"
                    required={true}
                    sx={{ bgcolor: "common.white" }}
                    className={classes.myInput}
                  ></TextField>
                </Box>
                <Box sx={{ mx: 4, my: 2 }}>
                  <TextField
                    fullWidth
                    variant="filled"
                    label="Your Message"
                    placeholder="Masukan Message Anda"
                    required={true}
                    multiline
                    rows={6}
                    // sx={{ border: "2px solid #363636" }}
                    // error={true}
                    type="email"
                    sx={{ bgcolor: "common.white" }}
                  ></TextField>
                </Box>
                <Box sx={{ mx: 4, my: 2 }}>
                  <Button
                    variant="contained"
                    size="large"
                    color="primary"
                    fullWidth
                    sx={{ fontWeight: "bold" }}
                  >
                    Send Message
                  </Button>
                </Box>
              </FormControl>
              <Box className={classes.myHeader}>
                <div className={classes.myLineLeft}></div>
                <Typography
                  variant="h6"
                  component="h6"
                  // helperText="Please Input Your Messege"
                  // gutterBottom
                  align="center"
                  className={classes.myButton}
                >
                  OR
                </Typography>
                <div className={classes.myLineRight}></div>
              </Box>
              <Box sx={{ m: 4 }}>
                <Button
                  variant="contained"
                  size="large"
                  color="secondary"
                  fullWidth
                  sx={{ fontWeight: "bold", color: "#FFFFFF" }}
                >
                  <WhatsAppIcon
                    color="#FFF"
                    fontSize="medium"
                    sx={{ paddingRight: 1 }}
                  />
                  WhatsApp
                </Button>
              </Box>
            </Grid>
            {/* <Grid item xs="auto">
                <Button 
                    variant="contained"
                    size="large"
                    color="primary"
                    sx={{ height: "100%", borderRadius: "2px" }}
                >
                    Send Message
                </Button>
                </Grid> */}
          </Grid>
        </Container>
      </Box>
      <Footer />
    </Box>
  );
}

export default Contact;
