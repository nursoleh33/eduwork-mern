import React from "react";
import Navbar from "../component/Navbar";
import Cards from "../component/Cards";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Compressor from "../assets/images/compressor.png";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Industrial from "../assets/images/industrial.png";
import Machine from "../assets/images/machine.png";
import Spindle from "../assets/images/spindle.png";
import Support from "../assets/images/support.png";
import Footer from "../parts/Footer";

function Service() {
  return (
    <Box>
      <Navbar />
      <Cards
        title="SERVICE"
        desc="Looking for an industrial maintenance and services company that provides various machine tools and compressors? Look no further than Sugi Inti Pekasa! Our team of experts provides top-quality services that are sure to meet your needs. Whether you need a new compressor or machine tool, we've got you covered. Contact us today to learn more!"
      />
      <Container maxWidth="lg" sx={{ pb: 8, pt: 7 }}>
        <Grid container alignItems="start" spacing={3}>
          <Grid item xs={12} lg={4} md={4}>
            <img src={Compressor} alt="Compressor" width="100%" height="auto" />
          </Grid>
          <Grid item xs={12} md={8} lg={8}>
            <Typography variant="h5" gutterBottom>
              Compressor Maintenance & Services
            </Typography>
            <Typography variant="body1" gutterBottom align="justify">
              Looking for a reliable compressor maintenance service provider?
              Look no further than Sugi Inti Pekasa! Our expert technicians can
              take care of all your compressor maintenance needs, ensuring that
              your equipment is always in top shape. Whether you need routine
              maintenance or repairs, we've got you covered. So don't wait any
              longer, contact us today to schedule an appointment!
            </Typography>
            <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                href="/contact"
                sx={{ fontWeight: "bold" }}
              >
                Contact Us
              </Button>
            </Box>
          </Grid>
          <Grid item xs={12} lg={4} md={4}>
            <img src={Machine} alt="Compressor" width="100%" height="auto" />
          </Grid>
          <Grid item xs={12} md={8} lg={8}>
            <Typography variant="h5" gutterBottom>
              Machine Tools Maintenance & Services
            </Typography>
            <Typography variant="body1" gutterBottom align="justify">
              Sugi Inti Pekasa is a machine tools maintenance and services
              provider. They provide various of machine tools maintenance and
              services. If you need any machine tools maintenance or services,
              then Sugi Inti Pekasa is the perfect place for you.
            </Typography>
            <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                href="/contact"
                sx={{ fontWeight: "bold" }}
              >
                Contact Us
              </Button>
            </Box>
          </Grid>
          <Grid item xs={12} lg={4} md={4}>
            <img src={Spindle} alt="Compressor" width="100%" height="auto" />
          </Grid>
          <Grid item xs={12} md={8} lg={8}>
            <Typography variant="h5" gutterBottom>
              Spindle Maintenance & Services
            </Typography>
            <Typography variant="body1" gutterBottom align="justify">
              Looking for a spindle machine maintenance and services company
              that you can trust? Look no further than Sugi Inti Pekasa! Our
              team of experts is dedicated to providing top-quality service to
              our customers. Whether you need repair work done or would like to
              purchase a new machine, we're here to help. Contact us today to
              learn more!
            </Typography>
            <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                href="/contact"
                sx={{ fontWeight: "bold" }}
              >
                Contact Us
              </Button>
            </Box>
          </Grid>
          <Grid item xs={12} lg={4} md={4}>
            <img src={Industrial} alt="Compressor" width="100%" height="auto" />
          </Grid>
          <Grid item xs={12} md={8} lg={8}>
            <Typography variant="h5" gutterBottom>
              Industrial & Consumer Panel Box
            </Typography>
            <Typography variant="body1" gutterBottom align="justify">
              Looking for a panel box that’s perfect for your industrial or
              consumer needs? Look no further than Sugi Inti Pekasa! Our various
              options are perfect for everything from consumer electronics to
              power tools. And best of all, they’re extremely affordable. So
              don’t wait any longer, order your Sugi Inti Pekasa panel box
              today!
            </Typography>
            <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                href="/contact"
                sx={{ fontWeight: "bold" }}
              >
                Contact Us
              </Button>
            </Box>
          </Grid>
          <Grid item xs={12} lg={4} md={4}>
            <img src={Support} alt="Compressor" width="100%" height="auto" />
          </Grid>
          <Grid item xs={12} md={8} lg={8}>
            <Typography variant="h5" gutterBottom>
              IT Support & Solutions
            </Typography>
            <Typography variant="body1" gutterBottom align="justify">
              Looking for a way to improve your business? Look no further than
              Sugi Inti Pekasa! We provide various of Industrial IT Support and
              solutions that are sure to help you take your business to the next
              level. Whether you need help with your ERP, INVENTORY, or
              WAREHOUSE, we've got you covered. So why wait? Give us a call
              today!
            </Typography>
            <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                href="/contact"
                sx={{ fontWeight: "bold" }}
              >
                Contact Us
              </Button>
            </Box>
          </Grid>
        </Grid>
      </Container>
      <Footer />
    </Box>
  );
}

export default Service;
