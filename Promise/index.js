//Kegunaan Promise
//1. untuk Mengatasi Callback Hell (Keserigan Menggunakan Higer Order Function /CallBack akan masuk kondisikedalam callback Hell, callback Hell tidak akan terjadi error juga , tapi masalah kerapian ajja)
//2. Menggunakan promise Membuat Kode Lebih Rapi
//3. Promise menjadi Alternatife Callback (bukan fungsi untuk mengatasi ascyronius, tapi sering diguankan untuk mengembalikan data yang dhasilkan dari asyncronus)

//kapan di butuhkan ketika kode yang kita buat dnegan callback sulit di baca

//callback Hell dalah keadaan dimana kita membuta callback di dalam callback terlalu banyak sehinga membuat kode jadi sulit di baca dan debug sulit (promise dibuat untuk mengatasi problem tersebut)

//ketika menggunakan fetch kembalian promise
//Promise ketika di buat akan menghasilkan pending, jika proses berhasil fullfyled(berhasil) dibuat/ di presentasikan di buat dengan fungsi callback resolve(kembalikan data) cara memanggilnya menggunakan then(menerima parameter result), Jika gagal mengembalikan rejected(digagalkan) bisa panggil dengan fungsi reject() dalam pemanggilannya dengan method catch(error)
//finally()-> akan selalu di eksekusi setelah semua selesai di jalankan ketika kondisi gagal atau berhasil s dapat digunakan untuk membuat efek laoding

//promise waib memilki 2 parameter
const janji = new Promise(function (resolve, reject) {
    if (true) {
        resolve('promise berhasil');
    } else {
        reject('promise gagal');
    }
});
janji.then(function (result) {
        console.log(result)
    }).catch(error => console.log(error))
    .finally(() => {
        console.log('promise selesai di eksekusi')
    })

const getData = (kondisi) => {
    return new Promise((resolve, reject) => {
        if (kondisi) {
            setTimeout(() => {
                resolve('Selamat datang di class MERN')
            }, 5000)
        } else {
            //kita dapat instansiasi untuk hadle error javascript
            // reject("maaf permitaan kamu tidak dapat di proses : (");
            reject(new Error("Mohon maaf permitaan kamu tidak dapat di proses : ("))
        }
    });
};

// document.getElementById('btn').addEventListener('click', function(){
//     this.innerHTML = 'Loading...';
//     let p = document.querySelector('p');
//     const data = getData(true); //mengembalikan promise, 

//     data.then((res)=>{
//         p.innerHTML = res; //kalau berhasil masukan pesan selamat datang
//     })
//     .catch(err => {
//         p.innerHTML = err;
//     })
//     .finally(()=>{
//         this.innerHTML = 'HI! Click Me';
//     })
// });

//Async Await 
//- Membuat Kode Asynchronous seolah terlihat seperti synchronous
//- Async Await tidak dapat di gunakan tanpa promise (then & catch), jika return bukan promise maka async await tdk dapat bekerja 
// - Asyns Await bukan cara membuat fungsi asingkronus
// - Async await hanya di gunakan bersama dengan pembuatan function, jadi pada saat pertama kali membuat harus di leatakan async await
// - Await bisa kita ibaratan sebagai alternative then, jadi kalau then mengembalikan callback result, jadi kita tidak perlu pakai then bisa langsung await untuk mengembalikan resultnya error atau berhasil

// Handle Error menggunakan Try catch (sebelumnya asnc menjadi sync)
document.getElementById('btn').addEventListener('click', async function () {
    this.innerHTML = "Loading....";
    const p = document.querySelector('p');
    //menggunakan try supaya code nya berjalan menjadi sync
    //sync tidak ada callback, maka kita perlu menangkap mengguakan try catch
    try {
        const data = await getData(false); //await akan menunggu proses getData Selama 5 detik, lalu data dapat  di tampilkan sama seperti syncronous
        //await dapat kita gunakan untuk mengambil result then 
        p.innerHTML = data;
    } catch (err) { //mengembalikan parameter
        // p.innerHTML = err;
        //untuk menangkap err
        // console.log(err.message)
        // p.innerHTML = (`${err.message}`)
        console.log(err)

    } finally {
        this.innerHTML = "HI! Click Me"
    }
})

fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json())
    .then(data => {
        document.getElementById('App').innerHTML = render(data)
    });

function render(result) {
    let tabel = `<table id="data-table" class="table table-success table-striped">
    </table>`;
    result.innerHTML = tabel;
    result.forEach(data => {
        tabel += `<tr>
        <td>${data.id}</td>
        <td>${data.name}</td>
        <td>${data.username}</td>
        <td>${data.email}</td>
        <td>
          ${data.address.street},
          ${data.address.suite}, 
          ${data.address.city}
         </td>
        <td>${data.phone}</td>
        <td>${data.website}</td>
        <td>${data.company.name}</td>
      </tr>`;
    })
    return tabel;
}
function message(msg){
    return `<tr>
    <td class="text-center" colspan="8">${msg}</td>
</tr>`
}